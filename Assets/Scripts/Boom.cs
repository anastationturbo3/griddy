using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour {
    public SpriteRenderer sprite;
    public int time = 0;
    public int max_time = 40;
    public Position pos;

    private Color c;

    void Awake(){
        pos = new Position((int)transform.position.x, (int)transform.position.y);
        sprite = GetComponent<SpriteRenderer>();
        DontDestroyOnLoad(this.gameObject);
    }

    void FixedUpdate(){
        time += 1;
        c = sprite.color;
        c.a = (1 - (float)time/(float)max_time);
        sprite.color = c;

        if(time == max_time) Destroy(this.gameObject);
    }
}
