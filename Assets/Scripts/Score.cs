﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Score : MonoBehaviour{
    public float timer;
    public float score;
    public float multiplier;
    public float temp_multiplier;
    public GameObject scoreText_obj;
    public GameObject levelText_obj;
    public GameObject multiText_obj;
    public GameObject comboText_obj;
    public GameObject pause_button;
    public Mask fill_mask;
    public GameObject circle;

    private float anim_time = 0f;
    public AnimationCurve color_wave;

    public bool awakened = false;

    public float[] high_scores;
    public GameObject final_scoreText_obj;
    public GameObject final_stageText_obj;
    public GameObject high_scoresText_obj;

    public bool simpleMode;

    public AudioClip new_high_score_sound;

    private int level_reached;
    private TMPro.TextMeshProUGUI scoreText;
    private TMPro.TextMeshProUGUI levelText;
    private TMPro.TextMeshProUGUI multiText;
    private TMPro.TextMeshProUGUI comboText;
    private TMPro.TextMeshProUGUI final_scoreText;
    private TMPro.TextMeshProUGUI final_stageText;
    private TMPro.TextMeshProUGUI high_scoresText;
    private Color c;
    private bool newHighScore;

    void Awake(){
        score = 0;
        multiplier = 1;
        timer = 0;
        scoreText = scoreText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        levelText = levelText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        multiText = multiText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        comboText = comboText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        final_scoreText = final_scoreText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        final_stageText = final_stageText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        high_scoresText = high_scoresText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        awakened = true;
        simpleMode = (PlayerPrefs.GetInt("GameMode", 0) == 0 ? true : false);
    }

    void FixedUpdate(){
        /*
        if(timer >= 7.9){
            temp_multiplier = 2*multiplier;
            c = multiText.color;
            c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
            c.r = 1;
            c.b = 81f/255f;
            c.g = 208f/255f;
            multiText.color = c;
        }
        else{
        }
            */
        temp_multiplier = multiplier*PlayerManager.instance.player_count;
        c = multiText.color;
        if(PlayerManager.instance.player_count < 2){
            c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
            c.r = 1;
            c.b = 1;
            c.g = 1;
            multiText_obj.GetComponent<Bobbin>().enable =false;
        }
        else {
            anim_time += Time.deltaTime;
            c.a = 1;
            c.r = 0.1254f + color_wave.Evaluate(anim_time);
            c.b = 0.9254f;
            c.g = 1;
            multiText_obj.GetComponent<Bobbin>().enable = true;
        }
        multiText.color = c;

        multiText.text = "\n\n\nx" + temp_multiplier;

        c = circle.GetComponent<Image>().color;
        c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
        circle.GetComponent<Image>().color = c;

        // c = circle_section.GetComponent<Image>().color;
        // c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
        // circle_section.GetComponent<Image>().color = c;

        c = comboText.color;
        c.a = (PlayerManager.instance.player_count < 2) ? Mathf.Max(0, Mathf.Min(timer/2, 1)) : 1;
        comboText.color = c;

        fill_mask.GetComponent<CutoutMask>().fillAmount = timer/10f - 1f/6f;

        if (timer > 0) timer -= 0.04f + multiplier*0.003f;
        if (timer <= 0){
            multiplier = 1;
            timer = 0;
        }
    }

    public void LevelTextUpdate(int level){
        level_reached = level;
        if(level%10 == 1 && level%100 != 11) levelText.text = level + "st Stage";
        else if(level%10 == 2 && level%100 != 12) levelText.text = level + "nd Stage";
        else if(level%10 == 3 && level%100 != 13) levelText.text = level + "rd Stage";
        else levelText.text = level + "th Stage";
    }

    public void KeepCombo(){
        if(!simpleMode)
            if(multiplier > 1) timer = 10;
    }

    public void ScorePlus(float count){
        if(!simpleMode){
            score += count*temp_multiplier;
            if(multiplier < 2) multiplier += 0.5f;
            else if(multiplier < 10) multiplier += 1f;
            timer = 10;
        }
        else{
            score += count*PlayerManager.instance.player_count;
        }

        scoreText.text = "Score: " + score;
    }

    public void ScoreEnd(){
        if(PlayerPrefs.GetInt("GameMode", 0) == 0){
            high_scores[0] = PlayerPrefs.GetFloat("SimpleScore_0", 0);
            high_scores[1] = PlayerPrefs.GetFloat("SimpleScore_1", 0);
            high_scores[2] = PlayerPrefs.GetFloat("SimpleScore_2", 0);
            high_scores[3] = PlayerPrefs.GetFloat("SimpleScore_3", 0);
            high_scores[4] = PlayerPrefs.GetFloat("SimpleScore_4", 0);
            high_scores[5] = PlayerPrefs.GetFloat("SimpleScore_5", 0);
            high_scores[6] = PlayerPrefs.GetFloat("SimpleScore_6", 0);
            high_scores[7] = PlayerPrefs.GetFloat("SimpleScore_7", 0);
            high_scores[8] = PlayerPrefs.GetFloat("SimpleScore_8", 0);
            high_scores[9] = PlayerPrefs.GetFloat("SimpleScore_9", 0);

            newHighScore = false;
            for(int i = 9; i>=0; i--){
                if(score < high_scores[i]) break;
                if(i<9) high_scores[i+1] = high_scores[i];
                high_scores[i] = score;
                if(i==0) newHighScore = true;
            }

            PlayerPrefs.SetFloat("SimpleScore_0", high_scores[0]);
            PlayerPrefs.SetFloat("SimpleScore_1", high_scores[1]);
            PlayerPrefs.SetFloat("SimpleScore_2", high_scores[2]);
            PlayerPrefs.SetFloat("SimpleScore_3", high_scores[3]);
            PlayerPrefs.SetFloat("SimpleScore_4", high_scores[4]);
            PlayerPrefs.SetFloat("SimpleScore_5", high_scores[5]);
            PlayerPrefs.SetFloat("SimpleScore_6", high_scores[6]);
            PlayerPrefs.SetFloat("SimpleScore_7", high_scores[7]);
            PlayerPrefs.SetFloat("SimpleScore_8", high_scores[8]);
            PlayerPrefs.SetFloat("SimpleScore_9", high_scores[9]);
        }
        else if(PlayerPrefs.GetInt("GameMode", 0) == 1){
            high_scores[0] = PlayerPrefs.GetFloat("HighScore_0", 0);
            high_scores[1] = PlayerPrefs.GetFloat("HighScore_1", 0);
            high_scores[2] = PlayerPrefs.GetFloat("HighScore_2", 0);
            high_scores[3] = PlayerPrefs.GetFloat("HighScore_3", 0);
            high_scores[4] = PlayerPrefs.GetFloat("HighScore_4", 0);
            high_scores[5] = PlayerPrefs.GetFloat("HighScore_5", 0);
            high_scores[6] = PlayerPrefs.GetFloat("HighScore_6", 0);
            high_scores[7] = PlayerPrefs.GetFloat("HighScore_7", 0);
            high_scores[8] = PlayerPrefs.GetFloat("HighScore_8", 0);
            high_scores[9] = PlayerPrefs.GetFloat("HighScore_9", 0);

            newHighScore = false;
            for(int i = 9; i>=0; i--){
                if(score < high_scores[i]) break;
                if(i<9) high_scores[i+1] = high_scores[i];
                high_scores[i] = score;
                if(i==0) newHighScore = true;
            }

            PlayerPrefs.SetFloat("HighScore_0", high_scores[0]);
            PlayerPrefs.SetFloat("HighScore_1", high_scores[1]);
            PlayerPrefs.SetFloat("HighScore_2", high_scores[2]);
            PlayerPrefs.SetFloat("HighScore_3", high_scores[3]);
            PlayerPrefs.SetFloat("HighScore_4", high_scores[4]);
            PlayerPrefs.SetFloat("HighScore_5", high_scores[5]);
            PlayerPrefs.SetFloat("HighScore_6", high_scores[6]);
            PlayerPrefs.SetFloat("HighScore_7", high_scores[7]);
            PlayerPrefs.SetFloat("HighScore_8", high_scores[8]);
            PlayerPrefs.SetFloat("HighScore_9", high_scores[9]);
        }
        else if(PlayerPrefs.GetInt("GameMode", 0) == 2){
            high_scores[0] = PlayerPrefs.GetFloat("VoidScore_0", 0);
            high_scores[1] = PlayerPrefs.GetFloat("VoidScore_1", 0);
            high_scores[2] = PlayerPrefs.GetFloat("VoidScore_2", 0);
            high_scores[3] = PlayerPrefs.GetFloat("VoidScore_3", 0);
            high_scores[4] = PlayerPrefs.GetFloat("VoidScore_4", 0);
            high_scores[5] = PlayerPrefs.GetFloat("VoidScore_5", 0);
            high_scores[6] = PlayerPrefs.GetFloat("VoidScore_6", 0);
            high_scores[7] = PlayerPrefs.GetFloat("VoidScore_7", 0);
            high_scores[8] = PlayerPrefs.GetFloat("VoidScore_8", 0);
            high_scores[9] = PlayerPrefs.GetFloat("VoidScore_9", 0);

            newHighScore = false;
            for(int i = 9; i>=0; i--){
                if(score < high_scores[i]) break;
                if(i<9) high_scores[i+1] = high_scores[i];
                high_scores[i] = score;
                if(i==0) newHighScore = true;
            }

            PlayerPrefs.SetFloat("VoidScore_0", high_scores[0]);
            PlayerPrefs.SetFloat("VoidScore_1", high_scores[1]);
            PlayerPrefs.SetFloat("VoidScore_2", high_scores[2]);
            PlayerPrefs.SetFloat("VoidScore_3", high_scores[3]);
            PlayerPrefs.SetFloat("VoidScore_4", high_scores[4]);
            PlayerPrefs.SetFloat("VoidScore_5", high_scores[5]);
            PlayerPrefs.SetFloat("VoidScore_6", high_scores[6]);
            PlayerPrefs.SetFloat("VoidScore_7", high_scores[7]);
            PlayerPrefs.SetFloat("VoidScore_8", high_scores[8]);
            PlayerPrefs.SetFloat("VoidScore_9", high_scores[9]);
        }

        c = final_scoreText.color;
        if(newHighScore == true){
            final_scoreText.text = "New High Score!\n" + score;
            c.r = 0.1254f + color_wave.Evaluate(anim_time);
            c.b = 0.9254f;
            c.g = 1;
            StartCoroutine(HighScoreDing());
        }
        else{
            final_scoreText.text = "Final Score\n" + score;
            c.r = 1;
            c.b = 1;
            c.g = 1;
        }
        final_scoreText.color = c;

        final_stageText.text = "Stage Reached\n" + level_reached;
        high_scoresText.text = "High Scores\n1. " + high_scores[0] + "\n2. " + high_scores[1] + "\n3. " + high_scores[2] + "\n4. " + high_scores[3] + "\n5. " + high_scores[4];
    }

    public IEnumerator HighScoreDing(){
        yield return new WaitForSeconds(1.15f);
        SoundManager.instance.PlaySound(new_high_score_sound);
    }
}
