﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCounter : MonoBehaviour{

    public GameObject cube_base;
    public GameObject[] cubes;
    public AnimationCurve color_light;

    private float time = 0f;
    private bool base_light;
    private SpriteRenderer base_sprite;

    private void Awake(){
        base_sprite = cube_base.GetComponent<SpriteRenderer>();
        CubesOff();
    }

    private void FixedUpdate(){
        time += Time.deltaTime;
        if(base_light){
            Color c = base_sprite.color;
            c.a = color_light.Evaluate(time);
            base_sprite.color = c;
        }
    }

    public void LightCube(int i){
        base_light = true;
        Color c = cubes[i].GetComponent<SpriteRenderer>().color;
        c.a = 1;
        cubes[i].GetComponent<SpriteRenderer>().color = c;
        StartCoroutine(SpriteFlash(cubes[i]));
    }

    public void CubesOff(){
        base_light = false;
        for(int i=0; i<4; i++){
            Color c = cubes[i].GetComponent<SpriteRenderer>().color;
            c.a = 0;
            cubes[i].GetComponent<SpriteRenderer>().color = c;
        }
    }

    public IEnumerator SpriteFlash(GameObject obj){
        SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
        int divisions = 20;
    
        Color starting_color = sprite.color;
        Color new_color = new Color(1, 1, 1, starting_color.a);
        float color_dif_r = (1-starting_color.r)/divisions;
        float color_dif_g = (1-starting_color.g)/divisions;
        float color_dif_b = (1-starting_color.b)/divisions;

        for(int i=0; i<divisions; i++){
            if(obj == null) yield break;
            sprite.color = new_color;
            new_color = new Color(new_color.r - color_dif_r, new_color.g - color_dif_g, new_color.b - color_dif_b, starting_color.a);
            yield return new WaitForFixedUpdate();
        }

        sprite.color = starting_color;
    }

    public IEnumerator SpriteFlash_Erase(GameObject obj){
        SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
        int divisions = 20;
    
        Color starting_color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
        Color new_color = new Color(1, 1, 1, 1);
        float color_dif_a = 1f/divisions;

        for(int i=0; i<divisions; i++){
            if(obj == null) yield break;
            sprite.color = new_color;
            new_color = new Color(1, 1, 1, new_color.a - color_dif_a);
            yield return new WaitForFixedUpdate();
        }

        sprite.color = starting_color;
    }
}
