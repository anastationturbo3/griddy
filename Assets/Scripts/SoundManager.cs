﻿using UnityEngine.Audio;
using UnityEngine;

public class SoundManager : MonoBehaviour{

    public static SoundManager instance = null;
    public Sound[] misc_sounds;

    public AudioSource main_audioSrc;
    public AudioSource single_audioSrc;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        /*
        float test;
        main_audioSrc.outputAudioMixerGroup.audioMixer.GetFloat("GlobalSoundVolume", out(test));
        Debug.Log(("S", PlayerPrefs.GetInt("Sound_Option"), test));
        main_audioSrc.outputAudioMixerGroup.audioMixer.SetFloat("GlobalSoundVolume", (PlayerPrefs.GetInt("Sound_Option")*10 - 100)*0.8f);
        */
    }

    public void PlaySound(AudioClip sound){
        main_audioSrc.PlayOneShot(sound);
    }

    public void PlaySingleSound(AudioClip sound){
        single_audioSrc.Stop();
        single_audioSrc.PlayOneShot(sound);
    }
}