﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {
    public static Loader instance = null;
    public GameObject gameManager;
    public GameObject soundManager;
    public GameObject playerManager;
    public GameObject musicManager;
    
    void Awake (){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(this);
        if (SoundManager.instance == null)
            Instantiate(soundManager);
        if (MusicManager.instance == null)
            Instantiate(musicManager);
        if (PlayerManager.instance == null)
            Instantiate(playerManager);
        if (GameManager.instance == null)
            Instantiate(gameManager);
    }
}
