﻿using UnityEngine.Audio;
using UnityEngine;

public class MusicManager : MonoBehaviour{

    public static MusicManager instance = null;
    public Sound[] misc_sounds;

    public AudioSource music_audioSrc;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        music_audioSrc.loop = true;
    }
 
    public void PlayMusic(AudioClip music){
        if(!music_audioSrc.isPlaying){
            music_audioSrc.Stop();
            music_audioSrc.clip = music;
            music_audioSrc.Play();
        }
    }

    public void PlayNewMusic(AudioClip music){
        music_audioSrc.Stop();
        music_audioSrc.clip = music;
        music_audioSrc.Play();
    }

    public void StopMusic(){
        music_audioSrc.Stop();
    }
}