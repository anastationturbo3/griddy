﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

    public static BoardManager instance = null;
    public GameObject floorTile;
    public GameObject wallTile;
    public GameObject bombaTile;
    //public GameObject[] wallCracked;
    public GameObject enemyTriangle;
    public GameObject enemySquare;
    public GameObject enemyPentagon;
    public GameObject enemyOctagon;
    public GameObject enemySquareSplit;
    public GameObject enemyTriangleSpawner;
    public GameObject enemyHexSpectre;
    //public GameObject enemySquareExplode;
    public GameObject HPpickup;
    public GameObject voidTile;
    public Transform boardHolder;

    public bool ExperimentalMode;

    private List<(int, int)> gridPositions = new List<(int, int)>();
    private List<(int, int)> early_gridPositions = new List<(int, int)>();
    private GameObject main_camera;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Initialise the Grids

    void InitialiseGrid(int level){
        for (int x = 0; x < GameManager.instance.max_cols; x++){
            for (int y = 0; y < GameManager.instance.max_rows; y++){
                if (GameManager.instance.SecretGrid[x, y].misc.Count > 0) { GameManager.instance.SecretGrid[x, y].misc.Clear(); }
            }
        }

        int cols = 3;
        int rows = 3;

        cols = Mathf.Max(Random.Range(3, Mathf.RoundToInt(GameManager.instance.max_cols / 2) + 1) - (level < 8 ? 4 - Mathf.RoundToInt(level / 2) : 0), Mathf.Max(PlayerManager.instance.GetMaxPlayerDistances_x() + PlayerManager.instance.player_count, 4 + PlayerManager.instance.player_count));
        rows = Mathf.Max(Random.Range(3, Mathf.RoundToInt(GameManager.instance.max_rows / 2) - 1) + Mathf.RoundToInt(GameManager.instance.max_rows / 2) - Mathf.RoundToInt(cols / 2) - (level < 8 ? 4 - Mathf.RoundToInt(level / 2) : 0), Mathf.Max(PlayerManager.instance.GetMaxPlayerDistances_y() + PlayerManager.instance.player_count, 2 + PlayerManager.instance.player_count));
        if (cols == 3 && rows == 3) rows = 5;

        if (Random.Range(0, 2) == 1){
            int temp = cols;
            cols = rows;
            rows = temp;
        }

        // Starting point: 2 limits for x and y:
        // 1 - x is between 1 and GameManager.instance.max_cols-cols-1
        // 2 - x is between GameManager.instance.PlayerPosition.x-cols+1 and GameManager.instance.PlayerPosition.x

        int starting_x = Random.Range(Mathf.Max(1, PlayerManager.instance.GetLimitFarthest_x() - cols + 1), Mathf.Min(GameManager.instance.max_cols - cols - 1, PlayerManager.instance.GetLimitClosest_x()) + 1);
        int starting_y = Random.Range(Mathf.Max(1, PlayerManager.instance.GetLimitFarthest_y() - rows + 1), Mathf.Min(GameManager.instance.max_rows - rows - 1, PlayerManager.instance.GetLimitClosest_y()) + 1);

        for (int x = 0; x < GameManager.instance.max_cols; x++){
            for (int y = 0; y < GameManager.instance.max_rows; y++){
                if (x >= starting_x && x <= starting_x + cols - 1 && y >= starting_y && y <= starting_y + rows - 1){
                    // gridPositions
                    early_gridPositions.Add((x, y));
                    // Secret Grid
                    GameManager.instance.SecretGrid[x, y].tile = Instantiate(floorTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].tile.transform.SetParent(boardHolder);
                }
                else{
                    // Secret Grid
                    GameManager.instance.SecretGrid[x, y].unit = Instantiate(voidTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].unit.transform.SetParent(boardHolder);
                }
            }
        }

        // Variations (25% chance)

        if (level > 4 && Random.Range(0, 4) == 0){
            if (rows > cols && starting_x > 0 && starting_x + rows < GameManager.instance.max_cols - 1){
                // Wall Rows
                for (int x = starting_x; x <= starting_x + cols - 1; x++){
                    int y = starting_y - 1;
                    GameManager.instance.SecretGrid[x, y].tile = Instantiate(floorTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x, y].unit);
                    GameManager.instance.SecretGrid[x, y].unit = Instantiate(wallTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].unit.transform.SetParent(boardHolder);

                    y = starting_y + rows;
                    GameManager.instance.SecretGrid[x, y].tile = Instantiate(floorTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x, y].unit);
                    GameManager.instance.SecretGrid[x, y].unit = Instantiate(wallTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].unit.transform.SetParent(boardHolder);
                }
            }
            else if (cols > rows && starting_y > 0 && starting_y + cols < GameManager.instance.max_rows - 1){
                // Wall Collumns
                for (int y = starting_y; y <= starting_y + rows - 1; y++){
                    int x = starting_x - 1;
                    GameManager.instance.SecretGrid[x, y].tile = Instantiate(floorTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x, y].unit);
                    GameManager.instance.SecretGrid[x, y].unit = Instantiate(wallTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].unit.transform.SetParent(boardHolder);

                    x = starting_x + cols;
                    GameManager.instance.SecretGrid[x, y].tile = Instantiate(floorTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x, y].unit);
                    GameManager.instance.SecretGrid[x, y].unit = Instantiate(wallTile, new Vector2(x, y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x, y].unit.transform.SetParent(boardHolder);
                }
            }
            /*
            else if(variation == 2 && starting_x > 1 && starting_x < GameManager.instance.max_cols-2 && starting_y > 1 && starting_y < GameManager.instance.max_rows-2){
                // Cross

                for(int x=starting_x; x<=starting_x+cols-1; x++){
                    int y = starting_y - 1;
                    early_gridPositions.Add((x,y));
                    GameManager.instance.SecretGrid[x,y].tile = Instantiate(floorTile, new Vector2(x,y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x,y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x,y].unit);
                    GameManager.instance.SecretGrid[x,y].unit = null;

                    y = starting_y + rows;
                    early_gridPositions.Add((x,y));
                    GameManager.instance.SecretGrid[x,y].tile = Instantiate(floorTile, new Vector2(x,y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x,y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x,y].unit);
                    GameManager.instance.SecretGrid[x,y].unit = null;
                }

                for(int y=starting_y; y<=starting_y+rows-1; y++){
                    int x = starting_x - 1;
                    early_gridPositions.Add((x,y));
                    GameManager.instance.SecretGrid[x,y].tile = Instantiate(floorTile, new Vector2(x,y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x,y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x,y].unit);
                    GameManager.instance.SecretGrid[x,y].unit = null;

                    x = starting_x + cols;
                    early_gridPositions.Add((x,y));
                    GameManager.instance.SecretGrid[x,y].tile = Instantiate(floorTile, new Vector2(x,y), Quaternion.identity) as GameObject;
                    GameManager.instance.SecretGrid[x,y].tile.transform.SetParent(boardHolder);
                    Destroy(GameManager.instance.SecretGrid[x,y].unit);
                    GameManager.instance.SecretGrid[x,y].unit = null;
                }
            }
            */
        }
    }

    // Get a valid random free space

    (int, int) RandomPosition(){
        if (gridPositions.Count == 0) return (-1, -1);
        int rand = Random.Range(0, gridPositions.Count);
        (int, int) randomPosition = gridPositions[rand];
        gridPositions.RemoveAt(rand);
        return randomPosition;
    }

    (int, int) RandomPosition_Early(){
        if (early_gridPositions.Count == 0) return (-1, -1);
        int rand = Random.Range(0, early_gridPositions.Count);
        (int, int) randomPosition = early_gridPositions[rand];
        early_gridPositions.RemoveAt(rand);
        return randomPosition;
    }

    (int, int) RandomPosition_AllAround(){
        List<(int, int)> temp_gridPositions = new List<(int, int)>(gridPositions);
        for (int i = 0; i < PlayerManager.instance.player_count; i++){
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y + 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y + 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y - 2));
        }

        if (temp_gridPositions.Count == 0) return (-1, -1);
        int rand = Random.Range(0, temp_gridPositions.Count);
        (int, int) randomPosition = temp_gridPositions[rand];

        gridPositions.Remove(randomPosition);
        return randomPosition;
    }

    (int, int) RandomPosition_Corner(){
        List<(int, int)> temp_gridPositions = new List<(int, int)>(gridPositions);
        for (int i = 0; i < PlayerManager.instance.player_count; i++){
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y + 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y + 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 2));
        }

        while (temp_gridPositions.Count > 0){
            int rand = Random.Range(0, temp_gridPositions.Count);
            (int, int) randomPosition = temp_gridPositions[rand];
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 + 1].unit != null){
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 + 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 + 1].unit.tag == "Destructable"){
                    if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 - 1].unit != null){
                        if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 - 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 - 1].unit.tag == "Destructable"){
                            if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 + 1].unit != null){
                                if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 + 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 + 1].unit.tag == "Destructable"){
                                    if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 - 1].unit != null){
                                        if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 - 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 - 1].unit.tag == "Destructable"){
                                            temp_gridPositions.RemoveAt(rand);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            gridPositions.Remove(randomPosition);
            return randomPosition;
        }

        return (-1, -1);
    }

    (int, int) RandomPosition_Orthogonal(){
        List<(int, int)> temp_gridPositions = new List<(int, int)>(gridPositions);
        for (int i = 0; i < PlayerManager.instance.player_count; i++){
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y - 2));
        }

        while (temp_gridPositions.Count > 0){
            int rand = Random.Range(0, temp_gridPositions.Count);
            (int, int) randomPosition = temp_gridPositions[rand];
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit != null){
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit.tag == "Destructable"){
                    if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit != null){
                        if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit.tag == "Destructable"){
                            if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit != null){
                                if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit.tag == "Destructable"){
                                    if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit != null){
                                        if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit.tag == "Destructable"){
                                            temp_gridPositions.RemoveAt(rand);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            gridPositions.Remove(randomPosition);
            return randomPosition;
        }

        return (-1, -1);
    }

    (int, int) RandomPosition_Worm(){
        List<(int, int)> temp_gridPositions = new List<(int, int)>(gridPositions);
        for (int i = 0; i < PlayerManager.instance.player_count; i++){
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            //temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 1));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 2, PlayerManager.instance.PlayerPositions[i].y));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y + 2));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 2, PlayerManager.instance.PlayerPositions[i].y));
            temp_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y - 2));
        }

        while (temp_gridPositions.Count > 0){
            int rand = Random.Range(0, temp_gridPositions.Count);
            (int, int) randomPosition = temp_gridPositions[rand];

            int adjacentWalls = 0;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit != null)
                adjacentWalls++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit != null)
                adjacentWalls++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit != null)
                adjacentWalls++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit != null)
                adjacentWalls++;

            if(adjacentWalls > 1){
                temp_gridPositions.RemoveAt(rand);
                continue;
            }

            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit != null){
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit.tag == "Destructable"){
                    if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit != null){
                        if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit.tag == "Destructable"){
                            if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit != null){
                                if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit.tag == "Destructable"){
                                    if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit != null){
                                        if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit.tag == "Wall" || GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit.tag == "Destructable"){
                                            temp_gridPositions.RemoveAt(rand);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            gridPositions.Remove(randomPosition);
            return randomPosition;
        }

        return (-1, -1);
    }

    (int, int) RandomPositionTrap(int dir_x, int dir_y){
        (int, int) randomPosition = (-1, -1);
        int yes = 1;
        while (yes == 1){
            yes = 0;
            if (gridPositions.Count == 0) return (-1, -1);
            int rand = Random.Range(0, gridPositions.Count);
            randomPosition = gridPositions[rand];
            gridPositions.RemoveAt(rand);
            // Not facing wall
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + dir_x, randomPosition.Item2 + dir_y].unit != null){
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + dir_x, randomPosition.Item2 + dir_y].unit.tag == "Wall"){
                    yes = 1;
                    continue;
                }
            }
            // Max 1 wall adjacent
            int wall_count = 0;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 + 1].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 + 1].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 - 1].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1 + 1, randomPosition.Item2 - 1].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 + 1].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1, randomPosition.Item2 - 1].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 + 1].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 + 1].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2].unit.tag == "Wall")
                    wall_count++;
            if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 - 1].unit != null)
                if (GameManager.instance.SecretGrid[randomPosition.Item1 - 1, randomPosition.Item2 - 1].unit.tag == "Wall")
                    wall_count++;
            if (wall_count > 1) yes = 1;
        }
        gridPositions.Remove((randomPosition.Item1 + 1, randomPosition.Item2 + 1));
        gridPositions.Remove((randomPosition.Item1 + 1, randomPosition.Item2));
        gridPositions.Remove((randomPosition.Item1 + 1, randomPosition.Item2 - 1));
        gridPositions.Remove((randomPosition.Item1, randomPosition.Item2 + 1));
        gridPositions.Remove((randomPosition.Item1, randomPosition.Item2 - 1));
        gridPositions.Remove((randomPosition.Item1 - 1, randomPosition.Item2 + 1));
        gridPositions.Remove((randomPosition.Item1 - 1, randomPosition.Item2));
        gridPositions.Remove((randomPosition.Item1 - 1, randomPosition.Item2 - 1));
        return randomPosition;
    }

    // Put objects at random

    void putRandomWalls(){
        /*
        if(Random.Range(0, 2) == 1){
            GameObject wall = wallTile;
            (int, int) pos = RandomPosition_Early();
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(wall, new Vector2(pos.Item1, pos.Item2), Quaternion.identity) as GameObject;
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(boardHolder);
        }
        else{
            GameObject wall = wallCracked[Random.Range(0, 3)];
            (int, int) pos = RandomPosition_Early();
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(wall, new Vector2(pos.Item1, pos.Item2), Quaternion.identity) as GameObject;
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(boardHolder);
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.Rotate(0, 0, 90*Random.Range(0, 4));
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.localScale = new Vector3(GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.localScale.x * (Random.Range(0, 2) == 0 ? 1 : -1), GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.localScale.y * (Random.Range(0, 2) == 0 ? 1 : -1), 1);
        }
        */

        (int, int) pos = RandomPosition_Early();
        if (pos.Item1 < 0 || pos.Item2 < 0) return;

        GameObject wall;
        /*
        if(ExperimentalMode && GameManager.instance.level > 7){
            if(Random.Range(0, 20) == 0){
                wall = bombaTile;
                GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(wall, new Vector2(pos.Item1, pos.Item2), Quaternion.identity) as GameObject;
                GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.GetComponent<Bomba>().position = new Position(pos.Item1, pos.Item2);
            }
            else{
                wall = wallTile;
                GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(wall, new Vector2(pos.Item1, pos.Item2), Quaternion.identity) as GameObject;
            }
            GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(boardHolder);
        }
        */

        wall = wallTile;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(wall, new Vector2(pos.Item1, pos.Item2), Quaternion.identity) as GameObject;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(boardHolder);
    }

    public void putRandomEnemy_AllAround(GameObject enemy){
        (int, int) pos = RandomPosition_AllAround();
        if (pos.Item1 < 0 || pos.Item2 < 0) return;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(enemy, new Vector2(pos.Item1, pos.Item2), Quaternion.identity);
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(GameManager.instance.Enemies.transform);
    }

    public void putRandomEnemy_Orthogonal(GameObject enemy){
        (int, int) pos = RandomPosition_Orthogonal();
        if (pos.Item1 < 0 || pos.Item2 < 0) return;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(enemy, new Vector2(pos.Item1, pos.Item2), Quaternion.identity);
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(GameManager.instance.Enemies.transform);
    }

    public void putRandomEnemy_Corner(GameObject enemy){
        (int, int) pos = RandomPosition_Corner();
        if (pos.Item1 < 0 || pos.Item2 < 0) return;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(enemy, new Vector2(pos.Item1, pos.Item2), Quaternion.identity);
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.transform.SetParent(GameManager.instance.Enemies.transform);
    }

    public void putRandomEnemy_Worm(){
        (int, int) pos = RandomPosition_Worm();
        if (pos.Item1 < 0 || pos.Item2 < 0) return;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit = Instantiate(enemyOctagon, new Vector2(pos.Item1, pos.Item2), Quaternion.identity);
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit.GetComponent<EnemyOctagon>().PromoteHead();

        // Slither and Quake
        GameObject last_object = GameManager.instance.SecretGrid[pos.Item1, pos.Item2].unit;
        (int, int) last_pos = pos;
        int segments = Random.Range(3, 6);
        while (segments > 0){
            List<(int, int)> possible_pos = new List<(int, int)>();
            if (GameManager.instance.SecretGrid[last_pos.Item1 + 1, last_pos.Item2].unit == null) possible_pos.Add((last_pos.Item1 + 1, last_pos.Item2));
            if (GameManager.instance.SecretGrid[last_pos.Item1 - 1, last_pos.Item2].unit == null) possible_pos.Add((last_pos.Item1 - 1, last_pos.Item2));
            if (GameManager.instance.SecretGrid[last_pos.Item1, last_pos.Item2 + 1].unit == null) possible_pos.Add((last_pos.Item1, last_pos.Item2 + 1));
            if (GameManager.instance.SecretGrid[last_pos.Item1, last_pos.Item2 - 1].unit == null) possible_pos.Add((last_pos.Item1, last_pos.Item2 - 1));
            if (possible_pos.Count == 0) { segments = 0; break; }

            int choice = 0;
            float dist = Mathf.Sqrt(Mathf.Pow(possible_pos[0].Item1 - PlayerManager.instance.GetClosestPlayer(last_pos.Item1, last_pos.Item2).x, 2) + Mathf.Pow(possible_pos[0].Item2 - PlayerManager.instance.GetClosestPlayer(last_pos.Item1, last_pos.Item2).y, 2));
            for (int i = 1; i < possible_pos.Count; i++){
                float new_dist = Mathf.Sqrt(Mathf.Pow(possible_pos[i].Item1 - PlayerManager.instance.GetClosestPlayer(last_pos.Item1, last_pos.Item2).x, 2) + Mathf.Pow(possible_pos[i].Item2 - PlayerManager.instance.GetClosestPlayer(last_pos.Item1, last_pos.Item2).y, 2));
                if (new_dist > dist){
                    dist = new_dist;
                    choice = i;
                }
            }

            GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit = Instantiate(enemyOctagon, new Vector2(possible_pos[choice].Item1, possible_pos[choice].Item2), Quaternion.identity);
            GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.transform.SetParent(GameManager.instance.Enemies.transform);
            GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<EnemyOctagon>().next_segment = last_object;
            GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<EnemyOctagon>().next_segment.GetComponent<EnemyOctagon>().previous_segment = GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit;
            GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<EnemyOctagon>().takeTurn = GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<EnemyOctagon>().next_segment.GetComponent<EnemyOctagon>().takeTurn;
            GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<SpriteRenderer>().sprite = GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<EnemyOctagon>().body_sprite;
            //GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit.GetComponent<EnemyOctagon>().mouth.SetActive(false);

            last_object = GameManager.instance.SecretGrid[possible_pos[choice].Item1, possible_pos[choice].Item2].unit;
            last_pos = (possible_pos[choice].Item1, possible_pos[choice].Item2);
            gridPositions.Remove(last_pos);
            segments -= 1;
            possible_pos.Clear();
        }
    }

    void putRandomMisc(GameObject misc){
        (int, int) pos = RandomPosition();
        if (pos.Item1 < 0 || pos.Item2 < 0) return;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].misc.Add(Instantiate(misc, new Vector3(pos.Item1, pos.Item2, 10), Quaternion.identity));
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].misc[0].transform.SetParent(boardHolder);
    }

    /*
    void putRandomTrap(GameObject misc, int dir_x, int dir_y){
        (int, int) pos = RandomPositionTrap(dir_x, dir_y);
        if (pos.Item1 < 0 || pos.Item2 < 0) return;
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].misc.Add(Instantiate(misc, new Vector2(pos.Item1, pos.Item2), Quaternion.identity));
        GameManager.instance.SecretGrid[pos.Item1, pos.Item2].misc[0].transform.SetParent(boardHolder);
    }
    */

    // Create all the things set above

    public void SetupScene(int level){
        boardHolder = new GameObject("Board").transform;

        main_camera = GameObject.FindGameObjectsWithTag("MainCamera")[0];
        main_camera.transform.position = new Vector3(GameManager.instance.max_cols / 2f, (GameManager.instance.max_rows / 2f) - 0.5f, -55);

        // Init enemy info

        List<EnemyInfo> EnemyChoices = new List<EnemyInfo>();
        EnemyChoices.Add(new EnemyInfo(enemySquare, 9, 2, 5, 1, 0, 1));
        EnemyChoices.Add(new EnemyInfo(enemyTriangle, 6, 2, 5, 1, 1, 1));
        EnemyChoices.Add(new EnemyInfo(enemySquareSplit, 6, 3, 3, 3, 0, 1));
        EnemyChoices.Add(new EnemyInfo(enemyPentagon, 4, 3, 3, 4, 2, 1));
        EnemyChoices.Add(new EnemyInfo(enemyTriangleSpawner, 3, 4, 1, 7, 1, 1));
        if(ExperimentalMode) EnemyChoices.Add(new EnemyInfo(enemyHexSpectre, 3, 4, 1, 9, 0, 1));
        //if(ExperimentalMode) EnemyChoices.Add(new EnemyInfo(enemyOctagon, 2, 5, 1, 10, 3, 3));
        //if(ExperimentalMode) EnemyChoices.Add(new EnemyInfo(enemyOctagon, 10, 5, 1, 1, 3, 3));
        //if(ExperimentalMode) EnemyChoices.Add(new EnemyInfo(enemySquareExplode, 1, 3, 1, 8, 0, 1));
        //if(ExperimentalMode) EnemyChoices.Add(new EnemyInfo(enemySquareExplode, 20, 3, 1, 1, 0, 1));

        // Some stuff in while loop for map validity
        bool nop = false;

        while (nop == false){
            gridPositions.Clear();
            early_gridPositions.Clear();
            foreach (EnemyInfo choice in EnemyChoices) choice.current_in_map = 0;

            if (GameManager.instance.level == 1){
                if(PlayerManager.instance.player_count == 0) GameManager.instance.SecretGrid[Mathf.RoundToInt(GameManager.instance.max_cols / 2), Mathf.RoundToInt(GameManager.instance.max_rows / 2)].unit = PlayerManager.instance.CreatePlayer(Mathf.RoundToInt(GameManager.instance.max_cols / 2), Mathf.RoundToInt(GameManager.instance.max_rows / 2));
                /*
                if(PlayerManager.instance.player_count == 0){
                    GameManager.instance.SecretGrid[Mathf.RoundToInt(GameManager.instance.max_cols / 2), Mathf.RoundToInt(GameManager.instance.max_rows / 2)].unit = PlayerManager.instance.CreatePlayer(Mathf.RoundToInt(GameManager.instance.max_cols / 2), Mathf.RoundToInt(GameManager.instance.max_rows / 2));
                    GameManager.instance.SecretGrid[Mathf.RoundToInt(GameManager.instance.max_cols / 2)+1, Mathf.RoundToInt(GameManager.instance.max_rows / 2)].unit = PlayerManager.instance.CreatePlayer(Mathf.RoundToInt(GameManager.instance.max_cols / 2)+1, Mathf.RoundToInt(GameManager.instance.max_rows / 2));
                }
                */
                else GameManager.instance.SecretGrid[Mathf.RoundToInt(GameManager.instance.max_cols / 2), Mathf.RoundToInt(GameManager.instance.max_rows / 2)].unit = PlayerManager.instance.Players[0];
            }
            else{
                for (int i = 0; i < PlayerManager.instance.player_count; i++)
                    GameManager.instance.SecretGrid[PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y].unit = PlayerManager.instance.Players[i];
            }
            // Floor & Void Setup
            InitialiseGrid(level);

            // Player Setup
            for (int i = 0; i < PlayerManager.instance.player_count; i++){
                early_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y));
                early_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y));
                early_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y - 1));
                early_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y));
                early_gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y + 1));
            }

            // Wall Setup
            int wall_min, wall_max, wall_rand;
            wall_min = Mathf.RoundToInt(early_gridPositions.Count / 5);
            wall_max = Mathf.RoundToInt(early_gridPositions.Count / 4);
            wall_rand = Random.Range(wall_min, wall_max + 1);
            for (int i = 0; i < wall_rand; i++) putRandomWalls();

            // Map Validity
            nop = true;
            for (int i = 0; i < PlayerManager.instance.player_count; i++){
                if (MapValid((PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y)) == false){
                    nop = false;
                    break;
                }
            }

            if (nop == false){
                for (int x = 0; x < GameManager.instance.max_cols; x++){
                    for (int y = 0; y < GameManager.instance.max_rows; y++){
                        if (GameManager.instance.SecretGrid[x, y].tile != null) { Destroy(GameManager.instance.SecretGrid[x, y].tile); GameManager.instance.SecretGrid[x, y].tile = null; }
                        if (GameManager.instance.SecretGrid[x, y].unit != null) if (GameManager.instance.SecretGrid[x, y].unit.tag != "Player") { Destroy(GameManager.instance.SecretGrid[x, y].unit); GameManager.instance.SecretGrid[x, y].unit = null; }
                        if (GameManager.instance.SecretGrid[x, y].misc.Count > 0) { for (int i = 0; i < GameManager.instance.SecretGrid[x, y].misc.Count; i++){ Destroy(GameManager.instance.SecretGrid[x, y].misc[i]); GameManager.instance.SecretGrid[x, y].misc = null;} GameManager.instance.SecretGrid[x, y].misc.Clear(); }
                    }
                }
                continue;
            }

            // Enemy Setup
            for (int i = 0; i < PlayerManager.instance.player_count; i++){
                gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y - 1));
                gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y + 1));
                gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y - 1));
                gridPositions.Remove((PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y + 1));
            }

            /*
            int max_enemies = Mathf.RoundToInt(gridPositions.Count / 6) + 2;
            int max_difficulty = level + 2;
            while(max_enemies > 0 && max_difficulty > 0){
                int enemy_rand = 0;
                if (max_difficulty < 4 || level < 3) enemy_rand = Random.Range(0, 5);
                else if(level < 5) Random.Range(0, 8);
                else enemy_rand = Random.Range(0, 9);
                if(enemy_rand == 0 || enemy_rand == 1 || enemy_rand == 2){putRandomEnemy_Orthogonal(enemySquare); max_difficulty -= 2;}
                else if(enemy_rand == 3 || enemy_rand == 4){putRandomEnemy_Corner(enemyTriangle); max_difficulty -= 2;}
                else if(enemy_rand == 5 || enemy_rand == 6){putRandomEnemy_Orthogonal(enemySquareSplit); max_difficulty -= 3;}
                else if(enemy_rand == 7){putRandomEnemy_AllAround(enemyPentagon); max_difficulty -= 4;}
                else if(enemy_rand == 8){putRandomEnemy_Corner(enemyTriangleSpawner); max_difficulty -= 5;}
                //else if(enemy_rand == 8){putRandomEnemy_Worm(); max_difficulty -= 5; max_enemies -= 1;}
                max_enemies -= 1;
            }
            */

            int max_enemies = Mathf.RoundToInt(gridPositions.Count / 6) + 2;
            int max_difficulty = level + 2;
            while (max_enemies > 0 && max_difficulty > 0){
                List<EnemyInfo> CurrentChoices = new List<EnemyInfo>();
                int cur_frequency = 0;
                foreach (EnemyInfo choice in EnemyChoices){
                    if (choice.current_in_map < choice.max_per_map && level >= choice.min_level){
                        CurrentChoices.Add(choice);
                        cur_frequency += choice.frequency;
                    }
                }

                int enemy_rand = Random.Range(0, cur_frequency + 1);
                while (enemy_rand > CurrentChoices[0].frequency){
                    enemy_rand -= CurrentChoices[0].frequency;
                    CurrentChoices.RemoveAt(0);
                }

                if (CurrentChoices.Count > 0){
                    CurrentChoices[0].SpawnEnemy();
                    max_enemies -= CurrentChoices[0].weight;
                    max_difficulty -= CurrentChoices[0].difficulty;
                    foreach (EnemyInfo choice in EnemyChoices){
                        if (choice.Enemy == CurrentChoices[0].Enemy) choice.current_in_map += 1;
                    }
                }
                else break;
            }

            // Enemy Validity
            if (GameManager.instance.Enemies.transform.childCount == 0){
                nop = false;
                for (int x = 0; x < GameManager.instance.max_cols; x++){
                    for (int y = 0; y < GameManager.instance.max_rows; y++){
                        if (GameManager.instance.SecretGrid[x, y].tile != null) { Destroy(GameManager.instance.SecretGrid[x, y].tile); GameManager.instance.SecretGrid[x, y].tile = null; }
                        if (GameManager.instance.SecretGrid[x, y].unit != null) if (GameManager.instance.SecretGrid[x, y].unit.tag != "Player") { Destroy(GameManager.instance.SecretGrid[x, y].unit); GameManager.instance.SecretGrid[x, y].unit = null; }
                        if (GameManager.instance.SecretGrid[x, y].misc.Count > 0) { for (int i = 0; i < GameManager.instance.SecretGrid[x, y].misc.Count; i++){ Destroy(GameManager.instance.SecretGrid[x, y].misc[i]); GameManager.instance.SecretGrid[x, y].misc = null;} GameManager.instance.SecretGrid[x, y].misc.Clear(); }
                    }
                }
                continue;
            }

            // Trap Setup
            /*
                        if(GameManager.instance.SecretGrid[GameManager.instance.PlayerPosition.x-1, GameManager.instance.PlayerPosition.y-1].unit==null) gridPositions.Add((GameManager.instance.PlayerPosition.x-1, GameManager.instance.PlayerPosition.y-1));
                        if(GameManager.instance.SecretGrid[GameManager.instance.PlayerPosition.x-1, GameManager.instance.PlayerPosition.y+1].unit==null) gridPositions.Add((GameManager.instance.PlayerPosition.x-1, GameManager.instance.PlayerPosition.y+1));
                        if(GameManager.instance.SecretGrid[GameManager.instance.PlayerPosition.x+1, GameManager.instance.PlayerPosition.y-1].unit==null) gridPositions.Add((GameManager.instance.PlayerPosition.x+1, GameManager.instance.PlayerPosition.y-1));
                        if(GameManager.instance.SecretGrid[GameManager.instance.PlayerPosition.x+1, GameManager.instance.PlayerPosition.y+1].unit==null) gridPositions.Add((GameManager.instance.PlayerPosition.x+1, GameManager.instance.PlayerPosition.y+1));

                        int trap_min, trap_max, trap_rand;
                        trap_min = -1;
                        trap_max = 3;
                        trap_rand = Random.Range(trap_min, trap_max+1);
                        for(int i=0; i<trap_rand; i++){
                            if(gridPositions.Count==0) break;
                            int rand = Random.Range(0, 4);
                            if(rand==0) putRandomTrap(trapUp, 0, 1);
                            if(rand==1) putRandomTrap(trapLeft, -1, 0);
                            if(rand==2) putRandomTrap(trapDown, 0, -1);
                            if(rand==3) putRandomTrap(trapRight, 1, 0);
                        }
            */

            // HP Seup
            if (level % 2 == 0 && PlayerManager.instance.player_count < 4){
                if (gridPositions.Count == 0){
                    nop = false;
                    for (int x = 0; x < GameManager.instance.max_cols; x++){
                        for (int y = 0; y < GameManager.instance.max_rows; y++){
                            if (GameManager.instance.SecretGrid[x, y].tile != null) { Destroy(GameManager.instance.SecretGrid[x, y].tile); GameManager.instance.SecretGrid[x, y].tile = null; }
                            if (GameManager.instance.SecretGrid[x, y].unit != null) if (GameManager.instance.SecretGrid[x, y].unit.tag != "Player") { Destroy(GameManager.instance.SecretGrid[x, y].unit); GameManager.instance.SecretGrid[x, y].unit = null; }
                            if (GameManager.instance.SecretGrid[x, y].misc.Count > 0) { for (int i = 0; i < GameManager.instance.SecretGrid[x, y].misc.Count; i++){ Destroy(GameManager.instance.SecretGrid[x, y].misc[i]); GameManager.instance.SecretGrid[x, y].misc = null;} GameManager.instance.SecretGrid[x, y].misc.Clear(); }
                        }
                    }
                    continue;
                }
                else putRandomMisc(HPpickup);
            }
        }
    }

    // BFS

    private bool MapValid((int, int) player_pos){
        List<(int, int)> Spaces = new List<(int, int)>();
        List<(int, int)> Already = new List<(int, int)>();
        Spaces.Add(player_pos);
        Already.Add(player_pos);
        int c = 0;

        //Start
        while (Spaces.Count > 0){
            c++;
            (int, int) cur = Spaces[0];

            if (GameManager.instance.SecretGrid[cur.Item1 + 1, cur.Item2].unit == null && !Already.Contains((cur.Item1 + 1, cur.Item2))) { Spaces.Add((cur.Item1 + 1, cur.Item2)); Already.Add((cur.Item1 + 1, cur.Item2)); }
            if (GameManager.instance.SecretGrid[cur.Item1 - 1, cur.Item2].unit == null && !Already.Contains((cur.Item1 - 1, cur.Item2))) { Spaces.Add((cur.Item1 - 1, cur.Item2)); Already.Add((cur.Item1 - 1, cur.Item2)); }
            if (GameManager.instance.SecretGrid[cur.Item1, cur.Item2 + 1].unit == null && !Already.Contains((cur.Item1, cur.Item2 + 1))) { Spaces.Add((cur.Item1, cur.Item2 + 1)); Already.Add((cur.Item1, cur.Item2 + 1)); }
            if (GameManager.instance.SecretGrid[cur.Item1, cur.Item2 - 1].unit == null && !Already.Contains((cur.Item1, cur.Item2 - 1))) { Spaces.Add((cur.Item1, cur.Item2 - 1)); Already.Add((cur.Item1, cur.Item2 - 1)); }
            Spaces.RemoveAt(0);

            //Special check
            bool cont = false;
            for (int i = 0; i < PlayerManager.instance.player_count; i++){
                if (cur == (PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y) || cur == (PlayerManager.instance.PlayerPositions[i].x + 1, PlayerManager.instance.PlayerPositions[i].y) || cur == (PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y + 1) || cur == (PlayerManager.instance.PlayerPositions[i].x - 1, PlayerManager.instance.PlayerPositions[i].y) || cur == (PlayerManager.instance.PlayerPositions[i].x, PlayerManager.instance.PlayerPositions[i].y - 1)){
                    cont = true;
                    break;
                }
            }

            if (cont) continue;
            else if (!gridPositions.Contains((cur.Item1, cur.Item2))) gridPositions.Add((cur.Item1, cur.Item2));
        }

        //Check
        if (c >= early_gridPositions.Count * 4 / 5) return true;
        return false;
    }

}