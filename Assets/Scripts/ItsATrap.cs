﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItsATrap : MonoBehaviour{
    public int push_x = 0;
    public int push_y = 0;

    public void Activate(MovingObject mo){
        mo.Move(push_x, push_y);
    }
}
