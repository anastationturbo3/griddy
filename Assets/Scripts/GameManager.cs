﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public Cell[,] SecretGrid;
    public int enemyCount = 0;
    public int max_cols = 15;
    public int max_rows = 11;
    public int level = 1;
    public int enemy_deaths_pending = 0;
    public float moveTime = .05f;
    public GameObject Enemies;
    public GameObject Enemies_End;
    public GameObject Particles;
    public GameObject Particles_Players;
    public List<GameObject> Bombs = new List<GameObject>();
    public bool BobmsExplode = true;
    public BoardManager boardScript;
    public Score scoreManager;
    public GameObject HealthCollector;
    public bool GamePaused = false;
    public bool GameOver = false;

    public AudioClip load_level_sound;
    public AudioClip pickup_sound;
    public AudioClip extra_sound;

    public AudioClip game_music_1;
    public AudioClip game_music_2;

    public bool playersTurn = true;

    [HideInInspector] public bool end = false;

    [SerializeField] private bool gameStart = false;
    [SerializeField] private bool doingSetup;
    private int health_points;

    // In the beginning, there was void

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        max_cols = 15;
        max_rows = 11;

        DontDestroyOnLoad(gameObject);
        boardScript = GetComponent<BoardManager>();
        scoreManager = GetComponent<Score>();
        end = false;
        enemy_deaths_pending = 0;
        GameManager.instance.SecretGrid = new Cell[max_cols, max_rows];
        for (int x=0; x<max_cols; x++){
            for (int y=0; y<max_rows; y++){
                GameManager.instance.SecretGrid[x,y] = new Cell();
            }
        }

        Particles = new GameObject ("Particles");
        DontDestroyOnLoad(Particles);

        Particles_Players = new GameObject ("Particles_Players");
        DontDestroyOnLoad(Particles_Players);

        Enemies_End = new GameObject ("Enemies_End");
        DontDestroyOnLoad(Enemies_End);

        HealthCollector =  Instantiate(HealthCollector, new Vector3((max_cols-1)/2, max_rows, 0), Quaternion.identity);
        DontDestroyOnLoad(HealthCollector);
        health_points = 0;

        if(PlayerPrefs.GetInt("Experimental_Option", 0) == 0) GetComponent<BoardManager>().ExperimentalMode = false;
        else GetComponent<BoardManager>().ExperimentalMode = true;

        StartCoroutine(ThisIsWhereTheFunBegins());
    }

    private IEnumerator ThisIsWhereTheFunBegins(){
        yield return new WaitUntil(() => scoreManager.awakened);
        InitGame();
    }

    void InitGame(){
        doingSetup = true;
        GamePaused = false;
        Enemies = new GameObject ("Enemies");
        enemyCount = 0;
        boardScript.SetupScene(level);
        GameManager.instance.scoreManager.LevelTextUpdate(level);
        playersTurn = true;
        enemy_deaths_pending = 0;
        doingSetup = false;
        if(level > 1){
            SoundManager.instance.PlaySound(load_level_sound);
            MusicManager.instance.PlayMusic(game_music_1);
        }
        else{
            SoundManager.instance.PlaySound(extra_sound);
            MusicManager.instance.PlayNewMusic(game_music_1);
        }
    }

    // Level control Stuff

    private void OnLevelWasLoaded(int index){
        if(gameStart == false) gameStart = true;
        else{
            level++;
            InitGame();
        }
    }

    // THE GAME

    async void Update(){
        // no play conditions
        if(doingSetup || playersTurn || PlayerManager.instance.player_count < 1 || enemy_deaths_pending > 0 || end)
            return;

        // check for victory
        if(Enemies.transform.childCount==0){
            Bombs.Clear();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else{
            List<GameObject> enemyList = new List<GameObject>();
            List<GameObject> actingList = new List<GameObject>();
            List<GameObject> panicList = new List<GameObject>();

            // get list of alive enemies & organise them
            for(int i=0; i<Enemies.transform.childCount; i++){
                enemyList = OrganiseEnemies(enemyList, Enemies.transform.GetChild(i).gameObject);
            }

            // - enemy turns: a 3(-ish) step process -

            // 1: switch the take / not take turn
            for(int i=0; i<enemyList.Count; i++){
                Enemy E = enemyList[i].GetComponent<Enemy>();
                if(E.GetComponent<Enemy>().to_die == true)
                    E.GetComponent<Enemy>().to_die = true;
                else if(E.GetComponent<Enemy>().sleep_short == true)
                    E.GetComponent<Enemy>().sleep_short = false;
                else if(E.GetComponent<Enemy>().takeTurn == true)
                    actingList.Add(enemyList[i]);
                else
                    E.TurnCounter();
            }

            enemyList.Clear();
            for(int i=0; i<actingList.Count; i++){
                //if(actingList[i].GetComponent<Enemy>().plannedMover) plannerList.Add(actingList[i]);
                
                enemyList.Add(actingList[i]);
            }

            // 1.5: enemies with planned moves first
            /*
            while(plannerList.Count>0 && end == false){
                panicList.Clear();
                encoreList.Clear();

                for(int i=0; i<plannerList.Count; i++){
                    Enemy E = plannerList[i].GetComponent<Enemy>();
                    int e = E.EnemyTurn();
                    if(e == 0)
                        panicList.Add(plannerList[i]);
                    else if(e == 2 && PlayerManager.instance.player_count == 0){
                        for(int j=0; j<PlayerManager.instance.Enemies_Recent.Count; j++){
                            PlayerManager.instance.Enemies_Recent[j].transform.SetParent(Enemies_End.transform);
                            if(PlayerManager.instance.Enemies_Recent[j].GetComponent<EnemyOctagon>() != null) PlayerManager.instance.Enemies_Recent[j].GetComponent<EnemyOctagon>().EndKeep();
                        }
                        end = true;
                        break;
                    }
                    else if(e == 3)
                        encoreList.Add(plannerList[i]);
                }

                if(panicList.Count == plannerList.Count) break;

                plannerList.Clear();
                for(int i=0; i<panicList.Count; i++)
                    plannerList.Add(panicList[i]);
                for(int i=0; i<encoreList.Count; i++)
                    plannerList.Add(encoreList[i]);
            }
            panicList.Clear();
            */

            // 2: enemies run normal, until none left can
            while(enemyList.Count>0 && end == false){
                panicList.Clear();

                for(int i=0; i<enemyList.Count; i++){
                    Enemy E = enemyList[i].GetComponent<Enemy>();
                    int e = E.EnemyTurn();
                    if(e == 0)
                        panicList.Add(enemyList[i]);
                    else if(e == 2 && PlayerManager.instance.player_count == 0){
                        for(int j=0; j<PlayerManager.instance.Enemies_Recent.Count; j++){
                            PlayerManager.instance.Enemies_Recent[j].transform.SetParent(Enemies_End.transform);
                            if(PlayerManager.instance.Enemies_Recent[j].GetComponent<EnemyOctagon>() != null) PlayerManager.instance.Enemies_Recent[j].GetComponent<EnemyOctagon>().EndKeep();
                        }
                        end = true;
                        break;
                    }
                }

                if(panicList.Count == enemyList.Count) break;

                enemyList.Clear();
                for(int i=0; i<panicList.Count; i++)
                    enemyList.Add(panicList[i]);
            }

            // 3: enemies do panic move, until none left can
            while(enemyList.Count>0 && end == false){
                panicList.Clear();

                for(int i=0; i<enemyList.Count; i++){
                    Enemy E = enemyList[i].GetComponent<Enemy>();
                    if(E.PanicTurn() == 0)
                        panicList.Add(enemyList[i]);
                }

                if(panicList.Count == enemyList.Count) break;

                enemyList.Clear();
                for(int i=0; i<panicList.Count; i++)
                    enemyList.Add(panicList[i]);
            }
        }

        // Bombs Away
        if(Enemies.transform.childCount > 0){
            BobmsExplode = false;
            foreach(GameObject bomb in Bombs){
                bomb.GetComponent<BombDiagonal>().Tick();
            }
            BobmsExplode = true;
        }

        // back to the player for the eternal cycle of life & death
        if(end == false) playersTurn = true;
        else StartCoroutine(WaitPlayerDeath());
    }

    private List<GameObject> OrganiseEnemies(List<GameObject> enemyList, GameObject enemy){
        enemyList.Add(enemy);

        int this_priority = enemy.GetComponent<Enemy>().priority;
        for(int i=enemyList.Count-2; i>=0; i--){
            
            int other_priority = enemyList[i].GetComponent<Enemy>().priority;
            if(this_priority < other_priority){
                GameObject temp_enemy = enemyList[i];
                enemyList[i] = enemyList[i+1];
                enemyList[i+1] = temp_enemy;
            }
            else break;
        }

        Position closest_player_pos = PlayerManager.instance.GetClosestPlayer(enemy.GetComponent<MovingObject>().position.x, enemy.GetComponent<MovingObject>().position.y);
        int this_dist = Mathf.Abs(closest_player_pos.x - enemy.GetComponent<MovingObject>().position.x) + Mathf.Abs(closest_player_pos.y - enemy.GetComponent<MovingObject>().position.y);
        for(int i=enemyList.Count-2; i>=0; i--){
            closest_player_pos = PlayerManager.instance.GetClosestPlayer(enemyList[i].GetComponent<MovingObject>().position.x, enemyList[i].GetComponent<MovingObject>().position.y);
            int other_dist = Mathf.Abs(closest_player_pos.x - enemyList[i].GetComponent<MovingObject>().position.x) + Mathf.Abs(closest_player_pos.y - enemyList[i].GetComponent<MovingObject>().position.y);
            if(this_dist < other_dist){
                GameObject temp_enemy = enemyList[i];
                enemyList[i] = enemyList[i+1];
                enemyList[i+1] = temp_enemy;
            }
            else break;
        }

        return enemyList;
    }

    public IEnumerator WaitPlayerDeath(){
        yield return new WaitUntil(() => PlayerManager.instance.PlayersWaitingExecution.Count == 0);
        for(int i=0; i<Enemies_End.transform.childCount; i++){
            yield return new WaitUntil(() => Enemies_End.transform.GetChild(i).GetComponent<Enemy>().kill_done == true);
        }

        GameOver = true;
        MusicManager.instance.StopMusic();

        for(int i=0; i<Enemies_End.transform.childCount; i++){
            if(Enemies_End.transform.GetChild(i).GetComponent<SpriteRenderer>() != null) Enemies_End.transform.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(255,255,255);
            foreach (Transform child in Enemies_End.transform.GetChild(i).transform)
                if(child.GetComponent<SpriteRenderer>() != null) child.GetComponent<SpriteRenderer>().color = new Color(255,255,255);
            if(Enemies_End.transform.GetChild(i).GetComponent<EnemyOctagon>() != null) Enemies_End.transform.GetChild(i).GetComponent<EnemyOctagon>().EndColor();
        }

        for(int i=0; i<Particles_Players.transform.childCount; i++){
            foreach(ParticleSystem ParticleSet in Particles_Players.transform.GetChild(i).GetComponentsInChildren<ParticleSystem>()){
                var col = ParticleSet.GetComponent<ParticleSystem>().colorOverLifetime;
                Gradient grad = new Gradient();
                grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.white, 0.0f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) } );
                col.color = grad;
            }
        }

        for(int i=0; i<Enemies.transform.childCount; i++){
            Destroy(Enemies.transform.GetChild(i).gameObject);
        }
        Destroy(GetComponent<BoardManager>().boardHolder.gameObject);
        Destroy(Particles);
        Destroy(HealthCollector);
        GetComponent<Score>().timer = 0;

        Color c = GetComponent<Score>().scoreText_obj.GetComponent<TMPro.TextMeshProUGUI>().color;
        c.a = 0;
        GetComponent<Score>().scoreText_obj.GetComponent<TMPro.TextMeshProUGUI>().color = c;

        c = GetComponent<Score>().levelText_obj.GetComponent<TMPro.TextMeshProUGUI>().color;
        c.a = 0;
        GetComponent<Score>().levelText_obj.GetComponent<TMPro.TextMeshProUGUI>().color = c;

        GetComponent<Score>().pause_button.SetActive(false);

        NoMoreLoad();
        GameManager.instance.scoreManager.ScoreEnd();
        StartCoroutine(GetComponent<ScoreScreen>().FadeToBlack());
        StartCoroutine(GetComponent<ScoreScreen>().ActivateScoreScreen());
    }

    public void PickedHealth(int x, int y){
        health_points += 1;
        
        if(health_points == 4){
            HealthCollector.GetComponent<HealthCounter>().CubesOff();
            health_points = 0;
            GameManager.instance.SecretGrid[x, y].unit = PlayerManager.instance.CreatePlayer(x, y);
            SoundManager.instance.PlaySound(extra_sound);
            GameManager.instance.SecretGrid[x, y].unit.GetComponent<Object>().SpriteFlash(1, 1, 1, 1);
            StartCoroutine(HealthCollector.GetComponent<HealthCounter>().SpriteFlash_Erase(HealthCollector.GetComponent<HealthCounter>().cube_base));
        }
        else{
            HealthCollector.GetComponent<HealthCounter>().LightCube(health_points-1);
            SoundManager.instance.PlaySound(pickup_sound);
        }
    }

    public void NoMoreLoad(){
        List<GameObject> result = new List<GameObject>();
 
        List<GameObject> rootGameObjectsExceptDontDestroyOnLoad = new List<GameObject>();
        for( int i = 0; i < SceneManager.sceneCount; i++ )
        {
            rootGameObjectsExceptDontDestroyOnLoad.AddRange( SceneManager.GetSceneAt( i ).GetRootGameObjects() );
        }
    
        List<GameObject> rootGameObjects = new List<GameObject>();
        Transform[] allTransforms = Resources.FindObjectsOfTypeAll<Transform>();
        for( int i = 0; i < allTransforms.Length; i++ )
        {
            Transform root = allTransforms[i].root;
            if( root.hideFlags == HideFlags.None && !rootGameObjects.Contains( root.gameObject ) )
            {
                rootGameObjects.Add( root.gameObject );
            }
        }
    
        for( int i = 0; i < rootGameObjects.Count; i++ )
        {
            if( !rootGameObjectsExceptDontDestroyOnLoad.Contains( rootGameObjects[i] ) )
                result.Add( rootGameObjects[i] );
        }

        for(int i=0; i<result.Count; i++){
            SceneManager.MoveGameObjectToScene(result[i], SceneManager.GetActiveScene());
        }
    }
}