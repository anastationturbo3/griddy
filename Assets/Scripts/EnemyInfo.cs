using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfo {
    public GameObject Enemy;
    public int frequency;
    public int difficulty;
    public int max_per_map;
    public int current_in_map;
    public int min_level;
    public int weight;

    private int type;

    public EnemyInfo(GameObject Enemy_, int frequency_, int difficulty_, int max_per_map_, int min_level_, int type_, int weight_){
        Enemy = Enemy_;
        frequency = frequency_;
        difficulty = difficulty_;
        max_per_map = max_per_map_;
        current_in_map = 0;
        min_level = min_level_;
        type = type_;
        weight = weight_;
    }

    public void SpawnEnemy(){
        if(type == 0) BoardManager.instance.putRandomEnemy_Orthogonal(Enemy);
        else if(type == 1) BoardManager.instance.putRandomEnemy_Corner(Enemy);
        else if(type == 2) BoardManager.instance.putRandomEnemy_AllAround(Enemy);
        else if(type == 3) BoardManager.instance.putRandomEnemy_Worm();
    }
}
