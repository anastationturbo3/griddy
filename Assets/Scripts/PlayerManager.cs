﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    
    public GameObject playerDiamond;
    public static PlayerManager instance = null;
    public int player_wait;
    public int player_count;
    public List<Position> PlayerPositions;
    public int cur_player;
    public (int, int) last_move;
    public List<GameObject> Players_Moving;
    public List<GameObject> Players_Attacking;
    public List<GameObject> Enemies_Recent;
    public bool attack_phase = false;

    public AudioClip move_sound;
    public AudioClip cant_move_sound;
    public AudioClip attack_sound;

    public List<GameObject> Players;
    public InputPlayer InputSystem;

    public GameObject Warnings;
    public GameObject WarningFlash;
    private int warning_wait;
    public int warning_wait_delay;

    public List<GameObject> PlayersWaitingExecution;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        InputSystem = new InputPlayer();
        InputSystem.Enable();

        player_wait = 0;
        player_count = 0;
        PlayerPositions = new List<Position>();
        Players = new List<GameObject>();

        Players_Moving = new List<GameObject>();
        Players_Attacking = new List<GameObject>();
        Enemies_Recent = new List<GameObject>();

        warning_wait = warning_wait_delay;
    }

    void LateUpdate(){
        if(!GameManager.instance.playersTurn || GameManager.instance.GamePaused || player_count < 1) return;

        int x_move = 0;
        int y_move = 0;
        //x_move = (int) Input.GetAxisRaw("Horizontal");
        //y_move = (int) Input.GetAxisRaw("Vertical");
        /*
        if(Input.GetButtonDown("left")) x_move -= 1;
        else if(Input.GetButtonDown("right")) x_move += 1;
        if(Input.GetButtonDown("down")) y_move -= 1;
        else if(Input.GetButtonDown("up")) y_move += 1;
        */
        if(InputSystem.PlayerMovement.MoveLeft.triggered) x_move -= 1;
        else if(InputSystem.PlayerMovement.MoveRight.triggered) x_move += 1;
        if(InputSystem.PlayerMovement.MoveDown.triggered) y_move -= 1;
        else if(InputSystem.PlayerMovement.MoveUp.triggered) y_move += 1;

        if(x_move != 0) y_move = 0;

        if(x_move !=0 || y_move !=0){
            List<GameObject> playerList = new List<GameObject>();
            for(int i=0; i<Players.Count; i++){
                playerList.Add(Players[i]);
            }

            int moved = 0;
            int last_moves = 0;
            int cur_sound_state = 0;

            // repeat player moves, until none left can
            while(playerList.Count>0){
                foreach(GameObject player in playerList){
                    cur_player = Players.FindIndex(a => a == player);
                    int cur_state = player.GetComponent<Player>().MoveCommand(x_move, y_move);
                    if(cur_state > 0){
                        moved += 1;
                        last_moves += 1;
                        if(cur_state == 1) Players_Moving.Add(player);
                        else if(cur_state == 2) Players_Attacking.Add(player);
                        if(cur_sound_state < cur_state) cur_sound_state = cur_state;
                    }
                }

                attack_phase = true;
                foreach(GameObject player in Players_Attacking){
                    cur_player = Players.FindIndex(a => a == player);
                    player.GetComponent<Player>().Move(x_move, y_move);
                    playerList.Remove(player);
                }
                attack_phase = false;

                foreach(GameObject player in Players_Moving){
                    cur_player = Players.FindIndex(a => a == player);
                    player.GetComponent<Player>().Move(x_move, y_move);
                    playerList.Remove(player);
                }

                Players_Moving.Clear();
                Players_Attacking.Clear();

                if(last_moves == 0) playerList.Clear();
                else last_moves = 0;
            }

            if(moved > 0){
                Enemies_Recent.Clear();
                last_move = (x_move, y_move);
                GameManager.instance.playersTurn = false;

                //Warnings
                warning_wait = warning_wait_delay;
                foreach (Transform child in Warnings.transform) Destroy(child.gameObject);
            }

            if(cur_sound_state == 0){
                StartCoroutine(Camera.main.GetComponent<CameraShake>().ShakeDirection(x_move, y_move));
                SoundManager.instance.PlaySound(cant_move_sound);
            }
            else if (cur_sound_state == 1){
                SoundManager.instance.PlaySound(move_sound);
            }
            else if (cur_sound_state == 2){
                StartCoroutine(Camera.main.GetComponent<CameraShake>().ShakeDirection(x_move, y_move));
                SoundManager.instance.PlaySound(attack_sound);
            }
        }
    }

    void Update(){
        // Players to be Executed
        for(int i=0; i<PlayersWaitingExecution.Count; i++){
            if(PlayersWaitingExecution[i] == null){
                PlayersWaitingExecution.RemoveAt(i);
                break;
            }
        }
    }

    void FixedUpdate(){
        // Handle Warnings
        if(PlayerPrefs.GetInt("Warning_Option", 0) == 0 || GameManager.instance.end == true){
            foreach (Transform child in Warnings.transform) Destroy(child.gameObject);
            return;
        }

        if(warning_wait > 0) warning_wait--;
        else if(warning_wait == 0){
            List<Position> WarningPositions = new List<Position>();
            List<Position> NullPositions = new List<Position>();

            foreach(Position pos in PlayerPositions){
                if(!WarningPositions.Contains(pos)) WarningPositions.Add(pos);
                if(!WarningPositions.Contains(new Position(pos.x+1, pos.y))) WarningPositions.Add(new Position(pos.x+1, pos.y));
                if(!WarningPositions.Contains(new Position(pos.x-1, pos.y))) WarningPositions.Add(new Position(pos.x-1, pos.y));
                if(!WarningPositions.Contains(new Position(pos.x, pos.y+1))) WarningPositions.Add(new Position(pos.x, pos.y+1));
                if(!WarningPositions.Contains(new Position(pos.x, pos.y-1))) WarningPositions.Add(new Position(pos.x, pos.y-1));
            }

            foreach(Position pos in WarningPositions){
                if(GameManager.instance.SecretGrid[pos.x, pos.y].unit != null){
                    if(!PlayerPositions.Contains(pos)){
                        NullPositions.Add(pos);
                        continue;
                    }
                }

                if(pos.x+1 < GameManager.instance.SecretGrid.GetLength(0) && GameManager.instance.SecretGrid[pos.x+1, pos.y].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x+1, pos.y].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x+1, pos.y].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x+1, pos.y].unit.GetComponent<Enemy>().enemyMoveType == "Orthogonal" || GameManager.instance.SecretGrid[pos.x+1, pos.y].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.x-1 > 0 && GameManager.instance.SecretGrid[pos.x-1, pos.y].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x-1, pos.y].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x-1, pos.y].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x-1, pos.y].unit.GetComponent<Enemy>().enemyMoveType == "Orthogonal" || GameManager.instance.SecretGrid[pos.x-1, pos.y].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.y+1 < GameManager.instance.SecretGrid.GetLength(1) && GameManager.instance.SecretGrid[pos.x, pos.y+1].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x, pos.y+1].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x, pos.y+1].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x, pos.y+1].unit.GetComponent<Enemy>().enemyMoveType == "Orthogonal" || GameManager.instance.SecretGrid[pos.x, pos.y+1].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.y-1 > 0 && GameManager.instance.SecretGrid[pos.x, pos.y-1].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x, pos.y-1].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x, pos.y-1].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x, pos.y-1].unit.GetComponent<Enemy>().enemyMoveType == "Orthogonal" || GameManager.instance.SecretGrid[pos.x, pos.y-1].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.x+1 < GameManager.instance.SecretGrid.GetLength(0) && pos.y+1 < GameManager.instance.SecretGrid.GetLength(1) && GameManager.instance.SecretGrid[pos.x+1, pos.y+1].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x+1, pos.y+1].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x+1, pos.y+1].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x+1, pos.y+1].unit.GetComponent<Enemy>().enemyMoveType == "Diagonal" || GameManager.instance.SecretGrid[pos.x+1, pos.y+1].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.x-1 > 0 && pos.y+1 < GameManager.instance.SecretGrid.GetLength(1) && GameManager.instance.SecretGrid[pos.x-1, pos.y+1].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x-1, pos.y+1].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x-1, pos.y+1].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x-1, pos.y+1].unit.GetComponent<Enemy>().enemyMoveType == "Diagonal" || GameManager.instance.SecretGrid[pos.x-1, pos.y+1].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.x+1 < GameManager.instance.SecretGrid.GetLength(0) && pos.y-1 > 0 && GameManager.instance.SecretGrid[pos.x+1, pos.y-1].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x+1, pos.y-1].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x+1, pos.y-1].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x+1, pos.y-1].unit.GetComponent<Enemy>().enemyMoveType == "Diagonal" || GameManager.instance.SecretGrid[pos.x+1, pos.y-1].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;
                if(pos.x-1 > 0 && pos.y-1 > 0 && GameManager.instance.SecretGrid[pos.x-1, pos.y-1].unit != null)
                    if(GameManager.instance.SecretGrid[pos.x-1, pos.y-1].unit.GetComponent<Enemy>() != null)
                        if(GameManager.instance.SecretGrid[pos.x-1, pos.y-1].unit.GetComponent<Enemy>().takeTurn == true && (GameManager.instance.SecretGrid[pos.x-1, pos.y-1].unit.GetComponent<Enemy>().enemyMoveType == "Diagonal" || GameManager.instance.SecretGrid[pos.x-1, pos.y-1].unit.GetComponent<Enemy>().enemyMoveType == "Octagonal"))
                            continue;

                NullPositions.Add(pos);
            }

            foreach(Position pos in NullPositions){
                WarningPositions.Remove(pos);
            }

            foreach(Position pos in WarningPositions){
                GameObject warning =  Instantiate(WarningFlash, new Vector3(pos.x, pos.y, -10), Quaternion.identity);
                warning.transform.SetParent(Warnings.transform);
            }

            warning_wait--;
        }
    }

    public GameObject CreatePlayer(int x, int y){
        GameObject newPlayer = Instantiate(playerDiamond, new Vector2(x, y), Quaternion.identity);
        Players.Add(newPlayer);
        player_count += 1;
        PlayerPositions.Add(new Position(x, y));
        return newPlayer;
    }
/*
    public GameObject PlacePlayer(int x, int y){
        GameObject newPlayer = Instantiate(playerDiamond, new Vector2(x, y), Quaternion.identity);
        Players.Add(newPlayer);
        return newPlayer;
    }

    public void ClearPlayers(){
        Players.Clear();
    }
*/
    public void RemovePlayer(GameObject player, GameObject enemy){
        PlayersWaitingExecution.Add(player);
        if(Enemies_Recent != null) PlayerManager.instance.Enemies_Recent.Add(enemy);
        GameManager.instance.SecretGrid[player.GetComponent<MovingObject>().position.x, player.GetComponent<MovingObject>().position.y].unit = null;
        Players.Remove(player);
        Players_Moving.Remove(player);
        player_count -= 1;
        for(int i=0; i<PlayerPositions.Count; i++){
            if((PlayerPositions[i].x, PlayerPositions[i].y) == (player.GetComponent<MovingObject>().position.x, player.GetComponent<MovingObject>().position.y)){
                PlayerPositions.RemoveAt(i);
                break;
            }
        }
        
        //PlayerPositions.Remove(player.GetComponent<MovingObject>().position);

        //if(player_count == 0) GameManager.instance.GameOver();
    }

    public Position GetClosestPlayer(int x, int y){
        Position closest = PlayerPositions[0];
        float dist = Mathf.Abs(x - PlayerPositions[0].x) + Mathf.Abs(y - PlayerPositions[0].y);
        for(int i=1; i<PlayerPositions.Count; i++){
            float new_dist = Mathf.Abs(x - PlayerPositions[i].x) + Mathf.Abs(y - PlayerPositions[i].y);
            if(new_dist < dist){
                closest = PlayerPositions[i];
                dist = new_dist;
            }
        }
        return closest;
    }

    public int GetMaxPlayerDistances_x(){
        int dist = 0;
        for(int i=0; i<PlayerPositions.Count; i++){
            for(int j=i+1; j<PlayerPositions.Count; j++){
                if(Mathf.Abs(PlayerPositions[i].x - PlayerPositions[j].x) > dist) dist = Mathf.Abs(PlayerPositions[i].x - PlayerPositions[j].x);
            }
        }
        return dist;
    }

    public int GetMaxPlayerDistances_y(){
        int dist = 0;
        for(int i=0; i<PlayerPositions.Count; i++){
            for(int j=i+1; j<PlayerPositions.Count; j++){
                if(Mathf.Abs(PlayerPositions[i].y - PlayerPositions[j].y) > dist) dist = Mathf.Abs(PlayerPositions[i].y - PlayerPositions[j].y);
            }
        }
        return dist;
    }

    public int GetLimitClosest_x(){
        int pos = GameManager.instance.max_cols;
        for(int i=0; i<PlayerPositions.Count; i++){
            if(PlayerPositions[i].x < pos) pos = PlayerPositions[i].x;
        }
        return pos;
    }

    public int GetLimitClosest_y(){
        int pos = GameManager.instance.max_rows;
        for(int i=0; i<PlayerPositions.Count; i++){
            if(PlayerPositions[i].y < pos) pos = PlayerPositions[i].y;
        }
        return pos;
    }

    public int GetLimitFarthest_x(){
        int pos = 0;
        for(int i=0; i<PlayerPositions.Count; i++){
            if(PlayerPositions[i].x > pos) pos = PlayerPositions[i].x;
        }
        return pos;
    }

    public int GetLimitFarthest_y(){
        int pos = 0;
        for(int i=0; i<PlayerPositions.Count; i++){
            if(PlayerPositions[i].y > pos) pos = PlayerPositions[i].y;
        }
        return pos;
    }
}
