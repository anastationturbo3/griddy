﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cell{
    public GameObject tile;
    public GameObject unit;
    public List<GameObject> misc;

    public Cell(){
        misc = new List<GameObject>();
    }
}
