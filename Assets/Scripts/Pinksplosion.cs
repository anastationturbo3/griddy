using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinksplosion : MonoBehaviour {
    public int step_count = -1;
    public int cur_step = 1;
    public int base_x;
    public int base_y;
    public GameObject pinkSquare;

    public AudioClip explosion_short_sound;
    public AudioClip explosion_end_sound;

    void FixedUpdate(){
        if(cur_step < step_count){
            if(base_x+cur_step < GameManager.instance.SecretGrid.GetLength(0) && base_y+cur_step < GameManager.instance.SecretGrid.GetLength(1)){
                if(GameManager.instance.SecretGrid[base_x+cur_step, base_y+cur_step].tile != null){
                    Instantiate(pinkSquare, new Vector3(base_x+cur_step, base_y+cur_step, 10), Quaternion.identity);
                }
            }
            
            if(base_x+cur_step < GameManager.instance.SecretGrid.GetLength(0) && base_y-cur_step >= 0){
                if(GameManager.instance.SecretGrid[base_x+cur_step, base_y-cur_step].tile != null){
                    Instantiate(pinkSquare, new Vector3(base_x+cur_step, base_y-cur_step, 10), Quaternion.identity);
                }
            }

            if(base_x-cur_step >= 0 && base_y+cur_step < GameManager.instance.SecretGrid.GetLength(1)){
                if(GameManager.instance.SecretGrid[base_x-cur_step, base_y+cur_step].tile != null){
                    Instantiate(pinkSquare, new Vector3(base_x-cur_step, base_y+cur_step, 10), Quaternion.identity);
                }
            }

            if(base_x-cur_step >= 0 && base_y-cur_step >= 0){
                if(GameManager.instance.SecretGrid[base_x-cur_step, base_y-cur_step].tile != null){
                    Instantiate(pinkSquare, new Vector3(base_x-cur_step, base_y-cur_step, 10), Quaternion.identity);
                }
            }

            cur_step++;
            if(cur_step == step_count) SoundManager.instance.PlaySingleSound(explosion_end_sound);
            else SoundManager.instance.PlaySingleSound(explosion_short_sound);
        }
        else if(this.transform.childCount == 0 && step_count == cur_step){
            Destroy(this.gameObject);
        }
    }
}
