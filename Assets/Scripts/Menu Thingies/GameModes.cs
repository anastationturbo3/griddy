using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameModes : MonoBehaviour {
    public void GameStart(){
        SceneManager.LoadScene("Game");
    }

    public void SimpleMode(){
        PlayerPrefs.SetInt("GameMode", 0);
    }

    public void ComboMode(){
        PlayerPrefs.SetInt("GameMode", 1);
    }

    public void VoidMode(){
        PlayerPrefs.SetInt("GameMode", 2);
    }
}
