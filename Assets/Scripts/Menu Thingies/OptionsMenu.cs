using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.EventSystems;

public class OptionsMenu : MonoBehaviour {
    public bool update_screen;
    public GameObject FullScreenToggle_obj;
    public GameObject Resolutions_obj;
    public GameObject Particles_obj;
    public GameObject Afterimage_obj;
    public GameObject Music_obj;
    public GameObject Sound_obj;
    public GameObject Warning_obj;
    public GameObject Experimental_obj;

    private Toggle FullScreenToggle;
    private Resolution[] resolutions;

    void Awake(){
        //bool update_screen = false;

        FullScreenToggle = FullScreenToggle_obj.GetComponent<Toggle>();
        FullScreenToggle.isOn = Screen.fullScreen;

        PlayerPrefs.SetInt("Resolutions_Option", 0);
        resolutions = Screen.resolutions;
        List<string> resolution_options = new List<string>();
        for(int i=0; i<resolutions.Length; i++){
            resolution_options.Add(resolutions[i].width+"x"+resolutions[i].height);
            if(string.Equals(resolutions[i].width+"x"+resolutions[i].height, Screen.currentResolution.width+"x"+Screen.currentResolution.height)) PlayerPrefs.SetInt("Resolutions_Option", i);
        }

        Resolutions_obj.GetComponent<OptionList>().ListOfThings = resolution_options;
        Resolutions_obj.GetComponent<OptionList>().cur_value = PlayerPrefs.GetInt("Resolutions_Option");
        Particles_obj.GetComponent<OptionList>().cur_value = PlayerPrefs.GetInt("Particles_Option");
        Afterimage_obj.GetComponent<OptionList>().cur_value = PlayerPrefs.GetInt("Afterimage_Option");
        Music_obj.GetComponent<OptionSoundSlider>().cur_value = PlayerPrefs.GetInt("Music_Option");
        Sound_obj.GetComponent<OptionSoundSlider>().cur_value = PlayerPrefs.GetInt("Sound_Option");
        Warning_obj.GetComponent<OptionToggle>().cur_value = PlayerPrefs.GetInt("Warning_Option");
        Experimental_obj.GetComponent<OptionToggle>().cur_value = PlayerPrefs.GetInt("Experimental_Option");
    }

    void Update(){
        if(update_screen == true){
            Screen.SetResolution(resolutions[Resolutions_obj.GetComponent<OptionList>().cur_value].width,
                                 resolutions[Resolutions_obj.GetComponent<OptionList>().cur_value].height,
                                 FullScreenToggle.isOn);
            update_screen = false;
        }
    }

    public void UpdateTheScreen(){
        update_screen = true;
    }

    public void UpdateResolutionOption(){
        PlayerPrefs.SetInt("Resolutions_Option", Resolutions_obj.GetComponent<OptionList>().cur_value);
    }

    public void UpdateParticlesOption(){
        PlayerPrefs.SetInt("Particles_Option", Particles_obj.GetComponent<OptionList>().cur_value);
    }

    public void UpdateAfterimageOption(){
        PlayerPrefs.SetInt("Afterimage_Option", Afterimage_obj.GetComponent<OptionList>().cur_value);
    }

    public void UpdateMusicOption(){
        PlayerPrefs.SetInt("Music_Option", Music_obj.GetComponent<OptionSoundSlider>().cur_value);
    }

    public void UpdateSoundOption(){
        PlayerPrefs.SetInt("Sound_Option", Sound_obj.GetComponent<OptionSoundSlider>().cur_value);
    }

    public void UpdateWarningOption(){
        PlayerPrefs.SetInt("Warning_Option", Warning_obj.GetComponent<OptionToggle>().cur_value);
    }

    public void UpdateExperimentalOption(){
        PlayerPrefs.SetInt("Experimental_Option", Experimental_obj.GetComponent<OptionToggle>().cur_value);
    }

    public void ResetScores(){
        PlayerPrefs.SetFloat("SimpleScore_0", 0);
        PlayerPrefs.SetFloat("SimpleScore_1", 0);
        PlayerPrefs.SetFloat("SimpleScore_2", 0);
        PlayerPrefs.SetFloat("SimpleScore_3", 0);
        PlayerPrefs.SetFloat("SimpleScore_4", 0);
        PlayerPrefs.SetFloat("SimpleScore_5", 0);
        PlayerPrefs.SetFloat("SimpleScore_6", 0);
        PlayerPrefs.SetFloat("SimpleScore_7", 0);
        PlayerPrefs.SetFloat("SimpleScore_8", 0);
        PlayerPrefs.SetFloat("SimpleScore_9", 0);

        PlayerPrefs.SetFloat("HighScore_0", 0);
        PlayerPrefs.SetFloat("HighScore_1", 0);
        PlayerPrefs.SetFloat("HighScore_2", 0);
        PlayerPrefs.SetFloat("HighScore_3", 0);
        PlayerPrefs.SetFloat("HighScore_4", 0);
        PlayerPrefs.SetFloat("HighScore_5", 0);
        PlayerPrefs.SetFloat("HighScore_6", 0);
        PlayerPrefs.SetFloat("HighScore_7", 0);
        PlayerPrefs.SetFloat("HighScore_8", 0);
        PlayerPrefs.SetFloat("HighScore_9", 0);

        PlayerPrefs.SetFloat("VoidScore_0", 0);
        PlayerPrefs.SetFloat("VoidScore_1", 0);
        PlayerPrefs.SetFloat("VoidScore_2", 0);
        PlayerPrefs.SetFloat("VoidScore_3", 0);
        PlayerPrefs.SetFloat("VoidScore_4", 0);
        PlayerPrefs.SetFloat("VoidScore_5", 0);
        PlayerPrefs.SetFloat("VoidScore_6", 0);
        PlayerPrefs.SetFloat("VoidScore_7", 0);
        PlayerPrefs.SetFloat("VoidScore_8", 0);
        PlayerPrefs.SetFloat("VoidScore_9", 0);
    }

    public void SelectForEventSystem(GameObject obj){
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(obj);
    }
}
