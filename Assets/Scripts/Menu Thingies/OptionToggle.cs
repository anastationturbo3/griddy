using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionToggle : MonoBehaviour{
    public int cur_value;
    public GameObject theToggle;
    public string setting;

    private Toggle button;

    void Awake(){
        button = theToggle.GetComponent<Toggle>();
        if(cur_value == 1) button.isOn = true;
        else button.isOn = false;
    }

    void Update(){
        if(button.isOn) cur_value = 1;
        else cur_value = 0;
        PlayerPrefs.SetInt(setting, cur_value);
    }
}
