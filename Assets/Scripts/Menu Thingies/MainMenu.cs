﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour{
    public GameObject s_scoresText_obj;
    public GameObject c_scoresText_obj;
    public GameObject v_scoresText_obj;

    public GameObject pointer_holder;
    public GameObject pointer_obj;
    public GameObject pointer;

    public float[] high_scores;

    public AudioClip menu_music;

    private TMPro.TextMeshProUGUI s_scoresText;
    private TMPro.TextMeshProUGUI c_scoresText;
    private TMPro.TextMeshProUGUI v_scoresText;
    private Coroutine last_coroutine = null;

    void OnLevelWasLoaded(){
        MusicManager.instance.PlayNewMusic(menu_music);
    }

    void Start(){
        if(PlayerPrefs.GetFloat("InitialStetup", 0) == 0){
            PlayerPrefs.SetFloat("SimpleScore_0", 0);
            PlayerPrefs.SetFloat("SimpleScore_1", 0);
            PlayerPrefs.SetFloat("SimpleScore_2", 0);
            PlayerPrefs.SetFloat("SimpleScore_3", 0);
            PlayerPrefs.SetFloat("SimpleScore_4", 0);
            PlayerPrefs.SetFloat("SimpleScore_5", 0);
            PlayerPrefs.SetFloat("SimpleScore_6", 0);
            PlayerPrefs.SetFloat("SimpleScore_7", 0);
            PlayerPrefs.SetFloat("SimpleScore_8", 0);
            PlayerPrefs.SetFloat("SimpleScore_9", 0);
            PlayerPrefs.SetFloat("HighScore_0", 0);
            PlayerPrefs.SetFloat("HighScore_1", 0);
            PlayerPrefs.SetFloat("HighScore_2", 0);
            PlayerPrefs.SetFloat("HighScore_3", 0);
            PlayerPrefs.SetFloat("HighScore_4", 0);
            PlayerPrefs.SetFloat("HighScore_5", 0);
            PlayerPrefs.SetFloat("HighScore_6", 0);
            PlayerPrefs.SetFloat("HighScore_7", 0);
            PlayerPrefs.SetFloat("HighScore_8", 0);
            PlayerPrefs.SetFloat("HighScore_9", 0);
            PlayerPrefs.SetFloat("VoidScore_0", 0);
            PlayerPrefs.SetFloat("VoidScore_1", 0);
            PlayerPrefs.SetFloat("VoidScore_2", 0);
            PlayerPrefs.SetFloat("VoidScore_3", 0);
            PlayerPrefs.SetFloat("VoidScore_4", 0);
            PlayerPrefs.SetFloat("VoidScore_5", 0);
            PlayerPrefs.SetFloat("VoidScore_6", 0);
            PlayerPrefs.SetFloat("VoidScore_7", 0);
            PlayerPrefs.SetFloat("VoidScore_8", 0);
            PlayerPrefs.SetFloat("VoidScore_9", 0);

            Resolution[] resolutions = Screen.resolutions;
            List<string> resolution_options = new List<string>();
            for(int i=0; i<resolutions.Length; i++){
                resolution_options.Add(resolutions[i].width + "x" + resolutions[i].height);
                if(resolutions[i].Equals(Screen.currentResolution)) PlayerPrefs.SetInt("Resolutions_Option", i);
            }

            Screen.fullScreen = true;
            PlayerPrefs.SetInt("Particles_Option", 2);
            PlayerPrefs.SetInt("Afterimage_Option", 0);
            PlayerPrefs.SetInt("Music_Option", 8);
            PlayerPrefs.SetInt("Sound_Option", 8);
            PlayerPrefs.SetInt("Warning_Option", 1);
            PlayerPrefs.SetInt("Experimental_Option", 0);
            PlayerPrefs.SetFloat("InitialStetup", 1);
        }
        
        if(MusicManager.instance.music_audioSrc != null) MusicManager.instance.PlayNewMusic(menu_music);
    }

    public void GameStart(){
        SceneManager.LoadScene("Game");
    }

    public void BackToMenu(){
        //ShowMouse();
        SceneManager.LoadScene("Menu");
    }

    public void Quit(){
        Application.Quit();
    }

    public void HighScores(){
        high_scores[0] = PlayerPrefs.GetFloat("SimpleScore_0", 0);
        high_scores[1] = PlayerPrefs.GetFloat("SimpleScore_1", 0);
        high_scores[2] = PlayerPrefs.GetFloat("SimpleScore_2", 0);
        high_scores[3] = PlayerPrefs.GetFloat("SimpleScore_3", 0);
        high_scores[4] = PlayerPrefs.GetFloat("SimpleScore_4", 0);
        high_scores[5] = PlayerPrefs.GetFloat("SimpleScore_5", 0);
        high_scores[6] = PlayerPrefs.GetFloat("SimpleScore_6", 0);
        high_scores[7] = PlayerPrefs.GetFloat("SimpleScore_7", 0);
        high_scores[8] = PlayerPrefs.GetFloat("SimpleScore_8", 0);
        high_scores[9] = PlayerPrefs.GetFloat("SimpleScore_9", 0);

        s_scoresText = s_scoresText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        s_scoresText.text = " 1. " + high_scores[0] + "\n 2. " + high_scores[1] + "\n 3. " + high_scores[2] + "\n 4. " + high_scores[3] + "\n 5. " + high_scores[4] + "\n 6. " + high_scores[5] + "\n 7. " + high_scores[6] + "\n 8. " + high_scores[7] + "\n 9. " + high_scores[8] + "\n10. " + high_scores[9];

        high_scores[0] = PlayerPrefs.GetFloat("HighScore_0", 0);
        high_scores[1] = PlayerPrefs.GetFloat("HighScore_1", 0);
        high_scores[2] = PlayerPrefs.GetFloat("HighScore_2", 0);
        high_scores[3] = PlayerPrefs.GetFloat("HighScore_3", 0);
        high_scores[4] = PlayerPrefs.GetFloat("HighScore_4", 0);
        high_scores[5] = PlayerPrefs.GetFloat("HighScore_5", 0);
        high_scores[6] = PlayerPrefs.GetFloat("HighScore_6", 0);
        high_scores[7] = PlayerPrefs.GetFloat("HighScore_7", 0);
        high_scores[8] = PlayerPrefs.GetFloat("HighScore_8", 0);
        high_scores[9] = PlayerPrefs.GetFloat("HighScore_9", 0);

        c_scoresText = c_scoresText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        c_scoresText.text = " 1. " + high_scores[0] + "\n 2. " + high_scores[1] + "\n 3. " + high_scores[2] + "\n 4. " + high_scores[3] + "\n 5. " + high_scores[4] + "\n 6. " + high_scores[5] + "\n 7. " + high_scores[6] + "\n 8. " + high_scores[7] + "\n 9. " + high_scores[8] + "\n10. " + high_scores[9];
    
        high_scores[0] = PlayerPrefs.GetFloat("VoidScore_0", 0);
        high_scores[1] = PlayerPrefs.GetFloat("VoidScore_1", 0);
        high_scores[2] = PlayerPrefs.GetFloat("VoidScore_2", 0);
        high_scores[3] = PlayerPrefs.GetFloat("VoidScore_3", 0);
        high_scores[4] = PlayerPrefs.GetFloat("VoidScore_4", 0);
        high_scores[5] = PlayerPrefs.GetFloat("VoidScore_5", 0);
        high_scores[6] = PlayerPrefs.GetFloat("VoidScore_6", 0);
        high_scores[7] = PlayerPrefs.GetFloat("VoidScore_7", 0);
        high_scores[8] = PlayerPrefs.GetFloat("VoidScore_8", 0);
        high_scores[9] = PlayerPrefs.GetFloat("VoidScore_9", 0);

        v_scoresText = v_scoresText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        v_scoresText.text = " 1. " + high_scores[0] + "\n 2. " + high_scores[1] + "\n 3. " + high_scores[2] + "\n 4. " + high_scores[3] + "\n 5. " + high_scores[4] + "\n 6. " + high_scores[5] + "\n 7. " + high_scores[6] + "\n 8. " + high_scores[7] + "\n 9. " + high_scores[8] + "\n10. " + high_scores[9];
    }

    public void SpawnPointer(float x, float y){
        if(pointer == null){
            pointer = Instantiate(pointer_obj, new Vector3 (x, y, pointer_obj.transform.position.z), Quaternion.identity);
            pointer.transform.SetParent(pointer_holder.transform);
        }
    }

    public void MovePointer(float x, float y){
        if(pointer == null) return;
        if(last_coroutine != null) StopCoroutine(last_coroutine);
        last_coroutine = StartCoroutine(SmoothMovement(new Vector3(pointer.transform.position.x, pointer.transform.position.y, pointer.transform.position.z), new Vector3(x, y, pointer.transform.position.z)));
    }

    protected IEnumerator SmoothMovement(Vector3 start, Vector3 Marker){
        Rigidbody2D rb2d = pointer.GetComponent<Rigidbody2D>();

        float sqrRemainingDistance = (pointer.transform.position - Marker).sqrMagnitude;
        while(sqrRemainingDistance > float.Epsilon){
            if(rb2d == null) break;
            Vector3 newPosition = Vector3.MoveTowards(rb2d.position, Marker, 1400 * Time.deltaTime);
            rb2d.MovePosition(newPosition);
            sqrRemainingDistance = (pointer.transform.position - Marker).sqrMagnitude;
            yield return new WaitForFixedUpdate();
        }
        last_coroutine = null;
    }

    public void RemovePointer(){
        if(pointer != null) Destroy(pointer);
        pointer = null;
    }
}