using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionSelectorConfirm : MonoBehaviour {
    public Button firstButton;
    public Button confirmButton;

    public void InvokeButton(){
        if(firstButton.isActiveAndEnabled) firstButton.onClick.Invoke();
        else if(confirmButton.isActiveAndEnabled) confirmButton.onClick.Invoke();
    }
}
