using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BackButton : MonoBehaviour {
    public GameObject theButton;
    public InputMenus InputSystem;

    [HideInInspector] public bool BackLock;

    void Awake(){
        BackLock = false;
    }

    void OnEnable(){
        InputSystem = new InputMenus();
        InputSystem.Enable();

        InputSystem.MenuControls.Back.performed += _ => PressBack();
    }

    private void PressBack(){
        if(theButton == null || BackLock) return;
        theButton.GetComponent<Button>().onClick.Invoke();
        /*
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(nextSelected);
        */
        /*
        EventSystem.current.GetComponent<ButtonTransition>().Transition(nextSelected);
        */
    }

    public void LockBackButton(bool set){
        BackLock = set;
    }

    void OnDisable(){
        InputSystem.Disable();
    }
}
