using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class InitializeOptions : MonoBehaviour {
    public AudioMixer musicMixer;
    public AudioMixer soundMixer;

    void Update(){
        // set fullscreen
        // set resolution
        // set particles
        musicMixer.SetFloat("GlobalSoundVolume", (PlayerPrefs.GetInt("Music_Option", 0)*10 - 100)*0.8f);
        soundMixer.SetFloat("GlobalSoundVolume", (PlayerPrefs.GetInt("Sound_Option", 0)*10 - 100)*0.8f);
    }
}