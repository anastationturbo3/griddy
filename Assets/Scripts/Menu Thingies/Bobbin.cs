using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobbin : MonoBehaviour {
    private float starting_x;
    public AnimationCurve x_management;
    private float starting_y;
    public AnimationCurve y_management;
    private float time = 0f;

    public bool enable;

    void Awake() {
        starting_x = transform.position.x;
        starting_y = transform.position.y;
    }

    void FixedUpdate(){
        time += Time.deltaTime;
        if(enable) transform.position = new Vector2(starting_x + x_management.Evaluate(time), starting_y + y_management.Evaluate(time));
        else transform.position = new Vector2(starting_x, starting_y);
    }
}
