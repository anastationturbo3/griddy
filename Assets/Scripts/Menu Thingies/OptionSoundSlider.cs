using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionSoundSlider : MonoBehaviour {
    public AudioMixer audioMixer;

    public int cur_value;
    public List<GameObject> Counters;
    public GameObject ButtonPrev;
    public GameObject ButtonNext;
    public AudioClip example_sound;
    public string global_audio;

    private int max_value;

    void Awake(){
        max_value = 10;
    }

    void Update(){
        if(cur_value == 0){
            ButtonPrev.SetActive(false);
            ButtonNext.SetActive(true);
        }
        else if(cur_value == max_value){
            ButtonPrev.SetActive(true);
            ButtonNext.SetActive(false);
        }
        else {
            ButtonPrev.SetActive(true);
            ButtonNext.SetActive(true);
        }

        for(int i=0; i<max_value; i++){
            if(i+1 <= cur_value) Counters[i].GetComponent<Image>().color = new Color (1,1,1,0.9f);
            else Counters[i].GetComponent<Image>().color = new Color (1,1,1,0.3f);
        }

        audioMixer.SetFloat("GlobalSoundVolume", (cur_value*10 - 100)*0.8f);
        PlayerPrefs.SetInt(global_audio, cur_value);
    }

    public void NextValue(){
        if(cur_value < max_value) cur_value += 1;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }

    public void PrevValue(){
        if(cur_value > 0) cur_value -= 1;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }

    public void SetValue_0(){
        cur_value = 0;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_1(){
        cur_value = 1;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_2(){
        cur_value = 2;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_3(){
        cur_value = 3;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_4(){
        cur_value = 4;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_5(){
        cur_value = 5;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_6(){
        cur_value = 6;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_7(){
        cur_value = 7;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_8(){
        cur_value = 8;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_9(){
        cur_value = 9;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
    
    public void SetValue_10(){
        cur_value = 10;
        if(example_sound != null) SoundManager.instance.PlaySound(example_sound);
    }
}
