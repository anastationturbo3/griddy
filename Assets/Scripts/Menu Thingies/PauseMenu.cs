﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour{

    public static PauseMenu instance;
    public GameObject PauseMenuUI;
    public InputPause InputSystem;
    public GameObject DarkPanel;
    public GameObject MainPauseMenu;

    void Awake(){
        InputSystem = new InputPause();
        InputSystem.Enable();
    }

    void LateUpdate(){
        //if(Input.GetButtonDown("pause") && GameManager.instance.GameOver == false){
        if(!GameManager.instance.GamePaused){
            if(InputSystem.MenuControls.Pause.triggered && !GameManager.instance.GameOver){
                PauseGame();
            }
        }
        else{
            if(InputSystem.MenuControls.Pause.triggered && MainPauseMenu.activeSelf && !GameManager.instance.GameOver){
                ResumeGame();
            }
        }
    }

    public void ResumeGame(){
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        EventSystem.current.SetSelectedGameObject(null);
        GameManager.instance.GamePaused = false;
    }

    public void PauseGame(){
        PauseMenuUI.SetActive(true);
        //ShowMouse();
        Time.timeScale = 0f;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(GetComponent<GeneralMenu>().keyboard_first_button);
        GameManager.instance.GamePaused = true;
    }

    public void setPanelSmall(){
        DarkPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(860, 1000);
    }

    public void setPanelBig(){
        DarkPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(1650, 1500);
    }

    public void GiveUp(){
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        EventSystem.current.SetSelectedGameObject(null);
        while(PlayerManager.instance.Players.Count > 0){
            PlayerManager.instance.Players[0].GetComponent<Player>().Die(0,0);
            PlayerManager.instance.RemovePlayer(PlayerManager.instance.Players[0], null);
        }
        GameManager.instance.end = true;
        GameManager.instance.GamePaused = false;
        StartCoroutine(GameManager.instance.WaitPlayerDeath());
    }

    public void EnableInput(){
        InputSystem.Enable();
    }

    public void DisableInput(){
        InputSystem.Disable();
    }
}
