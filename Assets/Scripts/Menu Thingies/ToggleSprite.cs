using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSprite : MonoBehaviour{
    private Image ToggleImage;
    private Toggle ToggleButton;

    void Awake(){
        ToggleImage = GetComponent<Image>();
        ToggleButton = GetComponentInParent<Toggle>();
    }

    void Update(){
        if(ToggleButton.isOn == true) ToggleImage.color = new Color(1,1,1,1);
        else  ToggleImage.color = new Color(1,1,1,0);
    }
}
