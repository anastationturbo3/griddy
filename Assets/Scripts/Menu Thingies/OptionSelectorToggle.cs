using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionSelectorToggle : MonoBehaviour {
    public Toggle toggle;

    public void InvokeToggle(){
        toggle.isOn = !toggle.isOn;
    }
}
