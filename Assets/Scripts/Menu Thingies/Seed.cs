using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Seed : MonoBehaviour {
    public TMP_Text ButtonText;
    private TMP_InputField Input;

    void Awake(){
        Input = GetComponent<TMP_InputField>();
    }

    public void SetSeed(){
        if(Input.text == "") SetRandomSeed();
        else { 
            Random.InitState(int.Parse(Input.text));
            ButtonText.text = Input.text;
        }
    }

    public void SetRandomSeed(){
        Random.InitState(System.Environment.TickCount);
        ButtonText.text = "ENTER SEED";
    }
}
