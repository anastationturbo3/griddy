using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bleeper : MonoBehaviour {
    public SpriteRenderer sprite;
    public int time = 0;
    public int max_time = 10;
    public int delay = 0;
    public float alpha_div = 2;
    public Position pos;

    private Color c;

    void Awake(){
        pos = new Position((int)transform.position.x, (int)transform.position.y);
        sprite = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate(){
        if(delay > 1) delay -= 1;
        else{
            time += 1;
            time %= max_time;

            c = sprite.color;
            c.a = (1 - (float)time/(float)max_time)/alpha_div;
            sprite.color = c;
        }
    }
}