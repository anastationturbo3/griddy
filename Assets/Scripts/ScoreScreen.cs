﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScoreScreen : MonoBehaviour {

    public GameObject black_panel;
    public GameObject score_panel_object;
    public GameObject score_panel_default_button;

    private int black_screen_transition_steps = 50;

    public IEnumerator FadeToBlack(){
        yield return new WaitForSeconds(1.15f);

        Image panel_image = black_panel.GetComponent<Image>();
        Color c;
        float color_dif_a = 1f/black_screen_transition_steps;

        for(int i=0; i<black_screen_transition_steps; i++){
            c = panel_image.color;
            c.a += color_dif_a;
            panel_image.color = c;
            yield return new WaitForFixedUpdate();
        }

        c = panel_image.color;
        c.a = 1;
        panel_image.color = c;
    }

/*
    public IEnumerator FadeFromBlack(){
        yield return new WaitForSeconds(1.15f);

        Image panel_image = black_panel.GetComponent<Image>();
        Color c;
        float color_dif_a = 1f/black_screen_transition_steps;

        for(int i=0; i<black_screen_transition_steps; i++){
            c = panel_image.color;
            c.a -= color_dif_a;
            panel_image.color = c;
            yield return new WaitForFixedUpdate();
        }

        c = panel_image.color;
        c.a = 0;
        panel_image.color = c;
    }
*/

    public IEnumerator ActivateScoreScreen(){
        yield return new WaitForSeconds(1.15f);

        //ShowMouse();

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(score_panel_default_button);
        CanvasGroup score_panel = score_panel_object.GetComponent<CanvasGroup>();

        score_panel.interactable = true;
        score_panel.blocksRaycasts = true;

        float a = 0f;
        float color_dif_a = 1f/black_screen_transition_steps;

        for(int i=0; i<black_screen_transition_steps; i++){
            a = score_panel.alpha;
            a += color_dif_a;
            score_panel.alpha = a;
            yield return new WaitForFixedUpdate();
        }

        score_panel.alpha = 1f;
    }
}
