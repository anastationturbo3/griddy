﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : Object{

    public Position position;
    public Position previous_position;
    public Coroutine last_coroutine = null;
    public GameObject AfterimageParticles;

    private float inverseMoveTime;
    private Rigidbody2D rb2d;
    
    protected virtual void Awake(){
        rb2d = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / GameManager.instance.moveTime;
        position = new Position((int)transform.position.x, (int)transform.position.y);
        previous_position = new Position(position.x, position.y);
    }

    protected IEnumerator SmoothMovement(Vector3 start, List <Vector3> Markers){
        while(Markers.Count>0){
            float sqrRemainingDistance = (transform.position - Markers[0]).sqrMagnitude;
            while(sqrRemainingDistance > float.Epsilon){
                Vector3 newPosition = Vector3.MoveTowards(rb2d.position, Markers[0], inverseMoveTime * Time.deltaTime * Markers.Count);
                rb2d.MovePosition(newPosition);
                sqrRemainingDistance = (transform.position - Markers[0]).sqrMagnitude;
                yield return new WaitForFixedUpdate();
            }
            Markers.RemoveAt(0);
        }
        last_coroutine = null;
    }

    public void Step(int xDir, int yDir){
            GameManager.instance.SecretGrid[position.x+xDir, position.y+yDir].unit = GameManager.instance.SecretGrid[position.x, position.y].unit;
            GameManager.instance.SecretGrid[position.x, position.y].unit = null;
            previous_position.x = position.x;
            position.x = position.x + xDir;
            previous_position.y = position.y;
            position.y = position.y + yDir;

            // interactable time
            List <Vector3> Markers = new List<Vector3>();
            /*
            for(int i=0; i<GameManager.instance.SecretGrid[position.x, position.y].misc.Count; i++){
                if(Markers.Count == 0) Markers.Add(new Vector3(position.x, position.y, transform.position.z));
                else if(Markers[0].x != position.x || Markers[0].y != position.y) Markers.Add(new Vector3(position.x, position.y, transform.position.z));
                GameManager.instance.SecretGrid[position.x, position.y].misc[i].GetComponent<ItsATrap>().Activate(this.GetComponent<MovingObject>());
            }
            */

            if(Markers.Count == 0) Markers.Add(new Vector3(position.x, position.y, transform.position.z));
            else if(Markers[0].x != position.x || Markers[0].y != position.y) Markers.Add(new Vector3(position.x, position.y, transform.position.z));
            if(last_coroutine != null) StopCoroutine(last_coroutine);
            last_coroutine = StartCoroutine(SmoothMovement(new Vector3(position.x-xDir, position.y-yDir, 0), Markers));
    }

    public virtual int Move(int xDir, int yDir){
        // Handled by either Player of Enemy
        return 0;
    }

    public override void Die(int xDir, int yDir){
        // Does nothing, player & enemy have scripts for dying
    }

}