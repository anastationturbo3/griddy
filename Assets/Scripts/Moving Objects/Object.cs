﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Object : MonoBehaviour{
    public virtual void Die(int xDir, int yDir){
        // Yes.
    }

    /*
    public IEnumerator SpriteFlash(){
        GameObject obj = this.gameObject;
        SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
        int divisions = 20;
    
        Color starting_color = sprite.color;
        Color new_color = new Color(1, 1, 1, 1);
        float color_dif_r = (1-starting_color.r)/divisions;
        float color_dif_g = (1-starting_color.g)/divisions;
        float color_dif_b = (1-starting_color.b)/divisions;
        float color_dif_a = (1-starting_color.a)/divisions;

        for(int i=0; i<divisions; i++){
            if(obj == null) yield break;
            if(obj.GetComponent<Enemy>() != null) if(obj.GetComponent<Enemy>().takeTurn == true){ sprite.color = starting_color; yield break; }
            sprite.color = new_color;
            new_color = new Color(new_color.r - color_dif_r, new_color.g - color_dif_g, new_color.b - color_dif_b, new_color.a - color_dif_a);
            yield return new WaitForFixedUpdate();
        }

        if(obj != null) sprite.color = starting_color;
    }
    */

    public void SpriteFlash(float r, float g, float b, float a){
        if(GetComponent<SpriteRenderer>() != null) GetComponent<SpriteRenderer>().color = new Color(r, g, b, a);
        if(GetComponent<ColorEnemy>() != null) GetComponent<ColorEnemy>().diff_rate = 0.01f;
        if(GetComponent<ColorPlayer>() != null) GetComponent<ColorPlayer>().diff_rate = 0.01f;

        foreach (Transform child in transform){
            if(child.gameObject.GetComponent<Object>() != null) child.gameObject.GetComponent<Object>().SpriteFlash(r, g, b, a);
        }
    }

    public virtual int ReturnExistance(){
        return 2;
    }
}
