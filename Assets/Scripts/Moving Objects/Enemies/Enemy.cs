﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public abstract class Enemy : MovingObject{
    public bool takeTurn;
    public bool spawned;
    public bool sleep_short;
    public float killDelay = 0.5f;
    public GameObject DeathParticles;
    public RotationEnemies rotator;
    public bool kill_done = false;
    public bool to_die = false;
    public string enemyMoveType;
    public int priority;

    protected override void Awake(){
        base.Awake();
        rotator = GetComponent<RotationEnemies>();
        int rand = Random.Range(0, 2);
        if (rand==0) takeTurn = false;
        else takeTurn = true;
        sleep_short = false;
        spawned = false;
    }

    public int Teleport(int target_x, int target_y){
        GameObject hit = GameManager.instance.SecretGrid[target_x, target_y].unit;
        if(hit == null){
            // Teleports Behind You
            TurnCounter();
            rotator.RotationBurstSmall();
            GameManager.instance.SecretGrid[target_x, target_y].unit = this.gameObject;

            previous_position.x = position.x;
            position.x = target_x;
            previous_position.y = position.y;
            position.y = target_y;
            
            transform.position = new Vector2(target_x, target_y);
            GameManager.instance.SecretGrid[position.x, position.y].unit.GetComponent<Object>().SpriteFlash(1, 1, 1, 1);
            return 1;
        }
        else if(hit.tag == "Player"){
            // wait untill player's move is done
            PlayerManager.instance.RemovePlayer(hit, this.gameObject);
            StartCoroutine(WaitAndKillandTeleport(hit, target_x, target_y));
            return 2;
        }
        // don't move
        return 0;
    }

    public override int Move(int xDir, int yDir){
        if(xDir==0 && yDir==0) return 0;
        GameObject hit = GameManager.instance.SecretGrid[position.x+xDir, position.y+yDir].unit;
        if(hit == null){
            // move
            Step(xDir, yDir);
            int particle_check = PlayerPrefs.GetInt("Afterimage_Option");
            if((particle_check==2 || particle_check==3) && AfterimageParticles != null){
                GameObject particels = Instantiate(AfterimageParticles, transform.position, Quaternion.identity);
                particels.transform.SetParent(this.transform);
            }
            TurnCounter();
            rotator.RotationBurst();
            return 1;
        }
        else if(hit.tag == "Player"){
            // wait untill player's move is done
            PlayerManager.instance.RemovePlayer(hit, this.gameObject);
            StartCoroutine(WaitAndKill(hit));
            return 2;
        }
        // don't move
        return 0;
    }

    public virtual int EnemyTurn(){
        (int, int) the_move = MoveEnemy();
        if (the_move == (0, 0)) return 0;
        return Move(the_move.Item1, the_move.Item2);
    }

    public int PanicTurn(){
        (int, int) the_move = PanicMove();
        if (the_move == (0, 0)) return 0;
        return Move(the_move.Item1, the_move.Item2);
    }

    public virtual bool TurnCounter(){
        // Depends on the enemy
        return false;
    }

    public virtual (int, int) MoveEnemy(){
        // The enemies will take care of it.
        return (0, 0);
    }

    public virtual (int, int) PanicMove(){
        // Each enemy has unique panic moves
        return (0, 0);
    }

    public override void Die(int xDir, int yDir){
        // score!
        if(spawned == true) GameManager.instance.scoreManager.KeepCombo();
        else GameManager.instance.scoreManager.ScorePlus(10);

        // particles
        foreach(ParticleSystem ParticleSet in DeathParticles.GetComponentsInChildren<ParticleSystem>()){
            var shape = ParticleSet.shape;
            shape.rotation = new Vector3(0, 0, (2*((xDir==-1) ? 1 : 0)+yDir)*90-(ParticleSet.shape.arc/2));
        }

        GameObject particels = Instantiate(DeathParticles, transform.position, Quaternion.identity);
        particels.transform.SetParent(GameManager.instance.Particles.transform);

        if(PlayerManager.instance.player_wait > 0) PlayerManager.instance.player_wait -= 1;
        GameManager.instance.enemy_deaths_pending += 1;
        StartCoroutine(WaitAndDie());
    }

    protected IEnumerator WaitAndKill(GameObject player){
        yield return new WaitUntil(() => player.GetComponent<MovingObject>().last_coroutine == null);
        rotator.RotationBurst();
        player.GetComponent<MovingObject>().Die(0, 0);

        if(PlayerManager.instance.player_count > 0) TurnCounter();
        /*
        else GetComponent<SpriteRenderer>().color = new Color(255,255,255);
        foreach (Transform child in transform)
            if(child.GetComponent<SpriteRenderer>() != null) child.GetComponent<SpriteRenderer>().color = new Color(255,255,255);
        */

        yield return new WaitUntil(() => player.GetComponent<Player>().dying_done == true);
        kill_done = true;
    }

    protected IEnumerator WaitAndKillandTeleport(GameObject player, int target_x, int target_y){
        yield return new WaitUntil(() => player.GetComponent<MovingObject>().last_coroutine == null);
        rotator.RotationBurst();
        GameManager.instance.SecretGrid[target_x, target_y].unit = GameManager.instance.SecretGrid[position.x, position.y].unit;
        GameManager.instance.SecretGrid[position.x, position.y].unit = null;
        previous_position.x = position.x;
        position.x = target_x;
        previous_position.y = position.y;
        position.y = target_y;
        transform.position = new Vector2(target_x, target_y); //??
        player.GetComponent<MovingObject>().Die(0, 0);

        if(PlayerManager.instance.player_count > 0) TurnCounter();
        /*
        else GetComponent<SpriteRenderer>().color = new Color(255,255,255);
        foreach (Transform child in transform)
            if(child.GetComponent<SpriteRenderer>() != null) child.GetComponent<SpriteRenderer>().color = new Color(255,255,255);
        */

        yield return new WaitUntil(() => player.GetComponent<Player>().dying_done == true);
        kill_done = true;
    }

    // Sort Stuff

    // Merges two subarrays of arr[]. 
    // First subarray is arr[l..m] 
    // Second subarray is arr[m+1..r]
    
    void merge(List<(Position, Position)> arr, int l, int m, int r) {
        int i, j, k; 
        int n1 = m - l + 1; 
        int n2 =  r - m; 
    
        /* create temp lists */
        List<(Position, Position)> L = new List<(Position, Position)>();
        List<(Position, Position)> R = new List<(Position, Position)>();
    
        /* Copy data to temp arrays L[] and R[] */
        for (i = 0; i < n1; i++) 
            L.Add(arr[l + i]); 
        for (j = 0; j < n2; j++) 
            R.Add(arr[m + 1 + j]); 
    
        /* Merge the temp arrays back into arr[l..r]*/
        i = 0;  // Initial index of first subarray 
        j = 0;  // Initial index of second subarray 
        k = l;  // Initial index of merged subarray 

        while (i < n1 && j < n2){
            Position closest_player = PlayerManager.instance.GetClosestPlayer(position.x, position.y);
            float distL = Mathf.Sqrt((L[i].Item1.x - closest_player.x)*(L[i].Item1.x - closest_player.x) + (L[i].Item1.y - closest_player.y)*(L[i].Item1.y - closest_player.y));
            float distR = Mathf.Sqrt((R[j].Item1.x - closest_player.x)*(R[j].Item1.x - closest_player.x) + (R[j].Item1.y - closest_player.y)*(R[j].Item1.y - closest_player.y));
            if (distL <= distR){
                arr[k] = L[i];
                i++;
            }
            else{
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        /* Copy the remaining elements of L[], if there are any */
        while (i < n1){
            arr[k] = L[i];
            i++;
            k++;
        }

        /* Copy the remaining elements of R[], if there are any */
        while (j < n2){
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    /* l is for left index and r is right index of the sub-array of arr to be sorted */
    void mergeSort(List<(Position, Position)> arr, int l, int r){ 
        if (l < r){ 
            // Same as (l+r)/2, but avoids overflow for 
            // large l and h 
            int m = l+(r-l)/2; 
    
            // Sort first and second halves 
            mergeSort(arr, l, m); 
            mergeSort(arr, m+1, r); 
    
            merge(arr, l, m, r); 
        } 
    }

    // The one true Merge
    public void mergin(List<(Position, Position)> arr){
        mergeSort(arr, 0, arr.Count-1);
    }

    protected IEnumerator WaitAndDie(){
        yield return new WaitUntil(() => GameManager.instance.playersTurn == false);

        GameManager.instance.enemy_deaths_pending -= 1;
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
