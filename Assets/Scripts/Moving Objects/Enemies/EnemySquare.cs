﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySquare : Enemy{
    private ColorEnemy color;

    protected override void Awake(){
        base.Awake();
        color = GetComponent<ColorEnemy>();
    }

    public override bool TurnCounter(){
        if(takeTurn == false){
            takeTurn = true;
            color.time = 0.75f;
            return false;
        }
        else{
            takeTurn = false;
            return true;
        }
    }

    public override (int, int) MoveEnemy(){
        List<(Position, Position)> toExplore = new List<(Position, Position)>();
        List<(int, int)> Already = new List<(int, int)>();
        Already.Add((position.x, position.y));

        // appetizers
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        // all the good stuff
        while(toExplore.Count > 0){
            // organise
            mergin(toExplore);
            foreach((Position, Position) P in toExplore){
                if(!Already.Contains((P.Item1.x, P.Item1.y))){
                    Already.Add((P.Item1.x, P.Item1.y));
                }
            }
            //Already.Add((toExplore[0].Item1.x, toExplore[0].Item1.y));
/*
            // trap check
            for(int i=0; i<GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count; i++){
                if(!Already.Contains((toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y), toExplore[0].Item2));
                }
            }
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count>0){
                toExplore.RemoveAt(0);
                continue;
            }
*/
            // do the boogaloo
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

            toExplore.RemoveAt(0);
        }

        // failed search
        return (0, 0);
    }

    public override (int, int) PanicMove(){
        List<(Position, Position)> PossibleMoves = new List<(Position, Position)>();

        // find possible moves
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        // put moves in order
        if(PossibleMoves.Count > 0){
            // organise
            mergin(PossibleMoves);
            return (PossibleMoves[0].Item2.x, PossibleMoves[0].Item2.y);
        }

        // failed search
        return (0, 0);
    }
}