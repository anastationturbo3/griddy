﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHexagon : Enemy {
    private ColorEnemy color;

    private bool invulnerable;
    private bool to_teleport;
    private bool keep_turn;

    /*
    public GameObject Piece_0;
    public GameObject Piece_1;
    public GameObject Piece_2;
    public GameObject Piece_3;
    */
    public GameObject[] segments;
    public Color base_color;
    public Color tele_color;
    public AudioClip teleport_sound;


    protected override void Awake(){
        base.Awake();
        rotator = GetComponent<RotationEnemies>();
        invulnerable = true;
        to_teleport = false;
        keep_turn = false;
    }

    public override bool TurnCounter(){
        if(takeTurn == false){
            takeTurn = true;
            for(int i=0; i<4; i++){
                color = segments[i].GetComponent<ColorEnemy>();
                color.time = 0.75f;
            }
            return false;
        }
        else{
            if(keep_turn) keep_turn = false;
            else takeTurn = false;
            return true;
        }
    }

    public override int EnemyTurn(){
        if(to_teleport == true){
            to_teleport = false;
            invulnerable = false;
            keep_turn = true;
            priority = 2;
            SoundManager.instance.PlaySound(teleport_sound);

            Position teleport_pos = new Position(position.x, position.y);
            while(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit != null){
                if(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<MovingObject>() == null) return 0;
                Position next_pos = GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<MovingObject>().previous_position;
                if(next_pos == teleport_pos) return 0;
                teleport_pos = next_pos;
            }

            return Teleport(teleport_pos.x, teleport_pos.y);
        }

        invulnerable = true;
        TeleModeOff();
        (int, int) the_move = MoveEnemy();
        if (the_move == (0,0)) return 0;
        to_teleport = false;
        return Move(the_move.Item1, the_move.Item2);
    }

    public override (int, int) MoveEnemy(){
        List<(Position, Position)> toExplore = new List<(Position, Position)>();
        List<(int, int)> Already = new List<(int, int)>();
        Already.Add((position.x, position.y));

        // appetizers
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        /*
        if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit.tag == "Player"){
                return (1, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y+1), new Position(1, 1)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit.tag == "Player"){
                return (-1, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y+1), new Position(-1, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit.tag == "Player"){
                return (1, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y-1), new Position(1, -1)));
        }

        if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit.tag == "Player"){
                return (-1, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y-1), new Position(-1, -1)));
        }
        */

        // all the good stuff
        while(toExplore.Count > 0){
            // organise
            mergin(toExplore);
            foreach((Position, Position) P in toExplore){
                if(!Already.Contains((P.Item1.x, P.Item1.y))){
                    Already.Add((P.Item1.x, P.Item1.y));
                }
            }
            //Already.Add((toExplore[0].Item1.x, toExplore[0].Item1.y));
/*
            // trap check
            for(int i=0; i<GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count; i++){
                if(!Already.Contains((toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y), toExplore[0].Item2));
                }
            }
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count>0){
                toExplore.RemoveAt(0);
                continue;
            }
*/
            // do the boogaloo
            /*
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }
            */

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

            toExplore.RemoveAt(0);
        }

        // failed search
        return (0, 0);
    }

    public override (int, int) PanicMove(){
        TeleModeOff();
        List<(Position, Position)> PossibleMoves = new List<(Position, Position)>();

        // find possible moves
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        /*
        if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit.tag == "Player"){
                return (1, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y+1), new Position(1, 1)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit.tag == "Player"){
                return (-1, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y+1), new Position(-1, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit.tag == "Player"){
                return (1, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y-1), new Position(1, -1)));
        }

        if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit.tag == "Player"){
                return (-1, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y-1), new Position(-1, -1)));
        }
        */

        // put moves in order
        if(PossibleMoves.Count > 0){
            // organise
            mergin(PossibleMoves);
            return (PossibleMoves[0].Item2.x, PossibleMoves[0].Item2.y);
        }

        // failed search
        return (0, 0);
    }

    public override int ReturnExistance(){
        if(!invulnerable) return 2;

        takeTurn = true;
        GameManager.instance.SecretGrid[position.x, position.y].unit = null;
        to_teleport = true;
        priority = 0;
        TeleModeOn();
        return 1;
    }

    private void TeleModeOn(){
        segments[0].transform.localPosition = new Vector2(0, 0);
        segments[0].GetComponent<ColorEnemy>().base_color = tele_color;
        segments[1].transform.localPosition = new Vector2(0, 0);
        segments[1].GetComponent<ColorEnemy>().base_color = tele_color;
        segments[2].transform.localPosition = new Vector2(0, 0);
        segments[2].GetComponent<ColorEnemy>().base_color = tele_color;
        segments[3].transform.localPosition = new Vector2(0, 0);
        segments[3].GetComponent<ColorEnemy>().base_color = tele_color;
    }

    private void TeleModeOff(){
        segments[0].transform.localPosition = new Vector2(-0.0333f, 0.0333f);
        segments[0].GetComponent<ColorEnemy>().base_color = base_color;
        segments[1].transform.localPosition = new Vector2(-0.0333f, -0.0333f);
        segments[1].GetComponent<ColorEnemy>().base_color = base_color;
        segments[2].transform.localPosition = new Vector2(0.0333f, -0.0333f);
        segments[2].GetComponent<ColorEnemy>().base_color = base_color;
        segments[3].transform.localPosition = new Vector2(0.0333f, 0.0333f);
        segments[3].GetComponent<ColorEnemy>().base_color = base_color;
    }
}