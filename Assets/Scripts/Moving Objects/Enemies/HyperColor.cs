﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HyperColor : MonoBehaviour {
    public AnimationCurve color_management;
    public Enemy enemy;
    public SpriteRenderer sprite;

    private float time = 0f;
    private Color colorFlicker;

    void Awake(){
        enemy = GetComponent<Enemy>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update(){
        time += Time.deltaTime;

        colorFlicker = sprite.color;
        //colorFlicker.gamma = color_management.Evaluate(time);
        sprite.color = colorFlicker;
    }
}