﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorEnemyHex : MonoBehaviour{
    public AnimationCurve color_inactive;
    public AnimationCurve color_active;
    public Enemy enemy;
    public SpriteRenderer sprite;
    public float time = 0f;
    public float offset;

    void Awake(){
        enemy = GetComponent<Enemy>();
        if(enemy == null) enemy = GetComponentInParent<Enemy>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate(){
        time += Time.deltaTime;
        Color c = sprite.color;
        if(enemy.takeTurn == false) c.a = color_inactive.Evaluate(time + offset);
        else c.a = color_active.Evaluate(time + offset);
        sprite.color = c;
    }
}