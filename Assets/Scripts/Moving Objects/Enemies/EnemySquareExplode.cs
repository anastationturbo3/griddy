﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySquareExplode : Enemy{
    private ColorEnemy color;
    private int max_explosion;
    public GameObject pinksplosion;

    protected override void Awake(){
        base.Awake();
        color = GetComponent<ColorEnemy>();
    }

    public override bool TurnCounter(){
        if(takeTurn == false){
            takeTurn = true;
            color.time = 0.75f;
            return false;
        }
        else{
            takeTurn = false;
            return true;
        }
    }

    public override (int, int) MoveEnemy(){
        List<(Position, Position)> toExplore = new List<(Position, Position)>();
        List<(int, int)> Already = new List<(int, int)>();
        Already.Add((position.x, position.y));

        // appetizers
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        // all the good stuff
        while(toExplore.Count > 0){
            // organise
            mergin(toExplore);
            foreach((Position, Position) P in toExplore){
                if(!Already.Contains((P.Item1.x, P.Item1.y))){
                    Already.Add((P.Item1.x, P.Item1.y));
                }
            }
            //Already.Add((toExplore[0].Item1.x, toExplore[0].Item1.y));
/*
            // trap check
            for(int i=0; i<GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count; i++){
                if(!Already.Contains((toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y), toExplore[0].Item2));
                }
            }
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count>0){
                toExplore.RemoveAt(0);
                continue;
            }
*/
            // do the boogaloo
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

            toExplore.RemoveAt(0);
        }

        // failed search
        return (0, 0);
    }

    public override (int, int) PanicMove(){
        List<(Position, Position)> PossibleMoves = new List<(Position, Position)>();

        // find possible moves
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        // put moves in order
        if(PossibleMoves.Count > 0){
            // organise
            mergin(PossibleMoves);
            return (PossibleMoves[0].Item2.x, PossibleMoves[0].Item2.y);
        }

        // failed search
        return (0, 0);
    }

    public override void Die(int xDir, int yDir){
        // score!
        GameManager.instance.scoreManager.ScorePlus(10);

        // particles
        foreach(ParticleSystem ParticleSet in DeathParticles.GetComponentsInChildren<ParticleSystem>()){
            var shape = ParticleSet.shape;
            shape.rotation = new Vector3(0, 0, (2*((xDir==-1) ? 1 : 0)+yDir)*90-(ParticleSet.shape.arc/2));
        }

        GameObject particels = Instantiate(DeathParticles, transform.position, Quaternion.identity);
        particels.transform.SetParent(GameManager.instance.Particles.transform);

        // GameObject bomb = Instantiate(pinksplosion, new Vector3(position.x, position.y, 10), Quaternion.identity);
        // GameManager.instance.SecretGrid[position.x, position.y].misc.Add(bomb);
        // bomb.GetComponent<BombDiagonal>().set = 1;

        // T.D.P: Tactical Diagonal Pinksplosion
        max_explosion = 0;

        int i=1;
        while(true){
            if(position.x+i >= GameManager.instance.SecretGrid.GetLength(0) || position.y+i >= GameManager.instance.SecretGrid.GetLength(1)) break;
            if(GameManager.instance.SecretGrid[position.x+i, position.y+i].tile == null) break;
            KillSpot(position.x+i, position.y+i);
            //Instantiate(pinksplosion, new Vector3(position.x+i, position.y+i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }
        
        i=1;
        while(true){
            if(position.x+i >= GameManager.instance.SecretGrid.GetLength(0) || position.y-i < 0) break;
            if(GameManager.instance.SecretGrid[position.x+i, position.y-i].tile == null) break;
            KillSpot(position.x+i, position.y-i);
            //Instantiate(pinksplosion, new Vector3(position.x+i, position.y-i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }

        i=1;
        while(true){
            if(position.x-i < 0 || position.y+i >= GameManager.instance.SecretGrid.GetLength(1)) break;
            if(GameManager.instance.SecretGrid[position.x-i, position.y+i].tile == null) break;
            KillSpot(position.x-i, position.y+i);
            //Instantiate(pinksplosion, new Vector3(position.x-i, position.y+i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }

        i=1;
        while(true){
            if(position.x-i < 0 || position.y-i < 0) break;
            if(GameManager.instance.SecretGrid[position.x-i, position.y-i].tile == null) break;
            KillSpot(position.x-i, position.y-i);
            //Instantiate(pinksplosion, new Vector3(position.x-i, position.y-i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }

        GameObject pink = Instantiate(pinksplosion, new Vector3(position.x, position.y, 10), Quaternion.identity);
        DontDestroyOnLoad(pink);
        pink.GetComponent<Pinksplosion>().base_x = position.x;
        pink.GetComponent<Pinksplosion>().base_y = position.y;
        pink.GetComponent<Pinksplosion>().step_count = max_explosion+1;

        if(PlayerManager.instance.player_wait > 0) PlayerManager.instance.player_wait -= 1;
        GameManager.instance.enemy_deaths_pending += 1;
        StartCoroutine(WaitAndDie());
    }

    public void KillSpot(int x, int y){
        GameObject hit = GameManager.instance.SecretGrid[x, y].unit;
        if(hit == null){ return; }
        else if(hit.CompareTag("Player")){ StartCoroutine(PlayerExplode(hit)); }
        else if(hit.CompareTag("Enemy")){ hit.GetComponent<Enemy>().to_die = true; hit.GetComponent<MovingObject>().Die(0, 0); }
        else if(hit.CompareTag("Destructable")){ hit.GetComponent<Destructable>().Die(0, 0); }
    }

    private IEnumerator PlayerExplode(GameObject player){
        yield return new WaitUntil(() => PlayerManager.instance.attack_phase == false);
        PlayerManager.instance.RemovePlayer(player, this.transform.parent.gameObject);
        player.GetComponent<MovingObject>().Die(0, 0);
    }
}
