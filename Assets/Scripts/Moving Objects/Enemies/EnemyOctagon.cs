﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOctagon : Enemy{
    private ColorEnemy color;
    public Sprite body_sprite;
    public Sprite head_sprite;
    public GameObject previous_segment;
    public GameObject next_segment;

    protected override void Awake(){
        base.Awake();
        color = GetComponent<ColorEnemy>();
    }

    public override bool TurnCounter(){
        if(next_segment == null){
            if(takeTurn == false){
                takeTurn = true;
                color.time = 0.75f;
                if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().TurnCounter();
                return false;
            }
            else{
                takeTurn = false;
                if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().TurnCounter();
                return true;
            }
        }
        else {
            takeTurn = next_segment.GetComponent<EnemyOctagon>().takeTurn;
            if(takeTurn == true) color.time = 0.75f;
            if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().TurnCounter();
            return !takeTurn;
        }
    }

    public override int Move (int xDir, int yDir){
        if(xDir==0 && yDir==0) return 0;
        GameObject hit = GameManager.instance.SecretGrid[position.x+xDir, position.y+yDir].unit;
        if(hit == null){
            // move
            Step(xDir, yDir);
            if(next_segment == null) TurnCounter();
            rotator.RotationBurst();

            // alert previous segment
            if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().Move((position.x - xDir) - previous_segment.GetComponent<EnemyOctagon>().position.x, (position.y - yDir) - previous_segment.GetComponent<EnemyOctagon>().position.y);
            return 1;
        }
        else if(hit.tag == "Player"){
            PlayerManager.instance.RemovePlayer(hit, this.gameObject);
            StartCoroutine(WaitAndKill(hit));
            return 2;
        }
        // don't move
        return 0;
    }

    public override (int, int) MoveEnemy(){
        if(next_segment == null){
            // is head

            List<(Position, Position)> toExplore = new List<(Position, Position)>();
            List<(int, int)> Already = new List<(int, int)>();
            Already.Add((position.x, position.y));

            // appetizers
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
                if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                    return (1, 0);
                }
            }
            else{
                toExplore.Add((new Position(position.x+1, position.y), new Position(1, 0)));
            }
            
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
                if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                    return (-1, 0);
                }
            }
            else{
                toExplore.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
            }

            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
                if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                    return (0, 1);
                }
            }
            else{
                toExplore.Add((new Position(position.x, position.y+1), new Position(0, 1)));
            }

            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
                if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                    return (0, -1);
                }
            }
            else{
                toExplore.Add((new Position(position.x, position.y-1), new Position(0, -1)));
            }

            // all the good stuff
            while(toExplore.Count > 0){
                // organise
                mergin(toExplore);
                foreach((Position, Position) P in toExplore){
                    if(!Already.Contains((P.Item1.x, P.Item1.y))){
                        Already.Add((P.Item1.x, P.Item1.y));
                    }
                }
                //Already.Add((toExplore[0].Item1.x, toExplore[0].Item1.y));
    /*
                // trap check
                for(int i=0; i<GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count; i++){
                    if(!Already.Contains((toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y))){
                        toExplore.Add((new Position(toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y), toExplore[0].Item2));
                    }
                }
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count>0){
                    toExplore.RemoveAt(0);
                    continue;
                }
    */
                // do the boogaloo
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

                toExplore.RemoveAt(0);
            }

            // failed search
            return (0, 0);
        }
        else{
            // is follower
            return (0, 0);
        }
    }

    public override (int, int) PanicMove(){
        if(next_segment == null){
            List<(Position, Position)> PossibleMoves = new List<(Position, Position)>();

            // find possible moves
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
                if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                    return (1, 0);
                }
            }
            else{
                PossibleMoves.Add((new Position(position.x+1, position.y), new Position(1, 0)));
            }
            
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
                if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                    return (-1, 0);
                }
            }
            else{
                PossibleMoves.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
            }

            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
                if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                    return (0, 1);
                }
            }
            else{
                PossibleMoves.Add((new Position(position.x, position.y+1), new Position(0, 1)));
            }

            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
                if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                    return (0, -1);
                }
            }
            else{
                PossibleMoves.Add((new Position(position.x, position.y-1), new Position(0, -1)));
            }

            // put moves in order
            if(PossibleMoves.Count > 0){
                // organise
                mergin(PossibleMoves);
                return (PossibleMoves[0].Item2.x, PossibleMoves[0].Item2.y);
            }

            // failed search
            return (0, 0);
        }
        else{
            // is follower
            return (0, 0);
        }
    }

    public void PromoteHead(){
        GetComponent<SpriteRenderer>().sprite = head_sprite;
        transform.SetParent(GameManager.instance.Enemies.transform);
        next_segment = null;
        enemyMoveType = "Orthogonal";
    }

    public void SleepChain(){
        //takeTurn = false;
        takeTurn = true;
        sleep_short = true;
        if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().SleepChain();
    }

    public override void Die(int xDir, int yDir){
        // score!
        GameManager.instance.scoreManager.ScorePlus(10);

        // give heads up to previous & next segment
        if(previous_segment != null){
            previous_segment.GetComponent<EnemyOctagon>().PromoteHead();
            previous_segment.GetComponent<EnemyOctagon>().SleepChain();
        }
        if(next_segment != null){
            next_segment.GetComponent<EnemyOctagon>().previous_segment = null;
        }

        // particles
        foreach(ParticleSystem ParticleSet in DeathParticles.GetComponentsInChildren<ParticleSystem>()){
            var shape = ParticleSet.shape;
            shape.rotation = new Vector3(0, 0, (2*((xDir==-1) ? 1 : 0)+yDir)*90-(ParticleSet.shape.arc/2));
        }

        GameObject particels = Instantiate(DeathParticles, transform.position, Quaternion.identity);
        particels.transform.SetParent(GameManager.instance.Particles.transform);

        if(PlayerManager.instance.player_wait > 0) PlayerManager.instance.player_wait -= 1;
        GameManager.instance.enemy_deaths_pending += 1;
        StartCoroutine(WaitAndDie());
    }

    public void EndKeep(){
        if(transform.parent != GameManager.instance.Enemies_End.transform){
            transform.SetParent(GameManager.instance.Enemies_End.transform);
            kill_done = true;
        }
        if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().EndKeep();
    }

    public void EndColor(){
        GetComponent<SpriteRenderer>().color = new Color(255,255,255);
        if(previous_segment != null) previous_segment.GetComponent<EnemyOctagon>().EndColor();
    }
}