﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationEnemyWorm : RotationEnemies {
    public EnemyOctagonDoubleMove worm;

    protected override void Awake(){
        worm = GetComponent<EnemyOctagonDoubleMove>();
    }

    protected override void FixedUpdate(){
        if(worm.next_segment == null){
            rotation_speed = 0;
            transform.rotation = new Quaternion(0,0,0,0);
        }
        else{
            if(worm.takeTurn == false){
                rotation_speed = rs_normal;
            }
            else rotation_speed = rs_fast;
            time += Time.deltaTime;
            transform.Rotate (Vector3.forward * (rotation_management.Evaluate(time)+1) * rotation_speed);
        }
    }
}