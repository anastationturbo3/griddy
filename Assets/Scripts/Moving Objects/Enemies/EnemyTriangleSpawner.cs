﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTriangleSpawner : Enemy {
    private ColorEnemy color;
    public GameObject enemyTriangle;
    public GameObject[] segments;
    public int activated_segments;

    public AudioClip spawn_sound;

    protected override void Awake(){
        base.Awake();
        color = GetComponent<ColorEnemy>();
        takeTurn = false;
        activated_segments = 0;
    }

    void FixedUpdate(){
        for(int i=0; i<activated_segments; i++){
            Color c = segments[i].GetComponent<SpriteRenderer>().color;
            c.a = color.color_active.Evaluate(color.time);
            segments[i].GetComponent<SpriteRenderer>().color = c;
        }

        for(int i=activated_segments; i<4; i++){
            Color c = segments[i].GetComponent<SpriteRenderer>().color;
            c.a = 0;
            segments[i].GetComponent<SpriteRenderer>().color = c;
        }
    }

    public override bool TurnCounter(){
        if(takeTurn == false){
            if(activated_segments < 4) activated_segments += 1;
            if(activated_segments == 4){
                takeTurn = true;
                color.time = 0.75f;
            }
            return false;
        }
        else{
            takeTurn = false;
            activated_segments = 0;
            return true;
        }
    }

    public override (int, int) MoveEnemy(){
        // Spawn enemy instead

        List<(Position, Position)> toSummon = new List<(Position, Position)>();

        // appetizers
        if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit == null)
            toSummon.Add((new Position(position.x+1, position.y+1), new Position(1, 1)));
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit == null)
            toSummon.Add((new Position(position.x-1, position.y+1), new Position(-1, 1)));

        if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit == null)
            toSummon.Add((new Position(position.x+1, position.y-1), new Position(1, -1)));

        if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit == null)
            toSummon.Add((new Position(position.x-1, position.y-1), new Position(-1, -1)));

        if(toSummon.Count > 0){
            Position closest_player = PlayerManager.instance.GetClosestPlayer(position.x, position.y);
            int cur_position = 0;
            float cur_distance = Mathf.Abs(toSummon[0].Item1.x - closest_player.x) + Mathf.Abs(toSummon[0].Item1.y - closest_player.y);
            for(int i=1; i<toSummon.Count; i++){
                float new_distance = Mathf.Abs(toSummon[i].Item1.x - closest_player.x) + Mathf.Abs(toSummon[i].Item1.y - closest_player.y);
                if(new_distance < cur_distance){
                    cur_distance = new_distance;
                    cur_position = i;
                }
            }

            // The Summoning
            GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit = Instantiate(enemyTriangle, new Vector2(toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y), Quaternion.identity);
            GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.transform.SetParent(GameManager.instance.Enemies.transform);
            GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.transform.GetComponent<Enemy>().takeTurn = false;
            GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.transform.GetComponent<Enemy>().spawned = true;

            SoundManager.instance.PlaySound(spawn_sound);
            Color color = GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.GetComponent<SpriteRenderer>().color;
            color.a = GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.GetComponent<ColorEnemy>().color_inactive.Evaluate(0);
            GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.GetComponent<SpriteRenderer>().color = color;
            GameManager.instance.SecretGrid[toSummon[cur_position].Item1.x, toSummon[cur_position].Item1.y].unit.GetComponent<Object>().SpriteFlash(1, 1, 1, 1);

            TurnCounter();
            rotator.RotationBurst();
            return (toSummon[cur_position].Item2.x, toSummon[cur_position].Item2.y);
        }

        return (0, 0);
    }

    public override (int, int) PanicMove(){
        // Do the enemy spawn again

        return MoveEnemy();
    }

    public override int EnemyTurn(){
        (int, int) the_move = MoveEnemy();
        GameObject hit = GameManager.instance.SecretGrid[position.x + the_move.Item1, position.y + the_move.Item2].unit;
        if (the_move == (0, 0)) return 0;
        /*
        else if(hit.tag == "Player"){
            // wait untill player's move is done
            PlayerManager.instance.RemovePlayer(hit, this.gameObject);
            StartCoroutine(WaitAndKill(hit));
            return 2;
        }
        */
        return 1;
    }
}