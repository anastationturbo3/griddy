﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine.UI;
using UnityEngine;

public class Player : MovingObject {
    public GameObject DeathParticles;
    public int wait;
    public AudioClip death_sound;
    public bool dying_done = false;

    private RotationPlayer rotator;

    protected override void Awake(){
        DontDestroyOnLoad(this.gameObject);
        base.Awake();
        rotator = GetComponent<RotationPlayer>();
    }

    public override int Move (int xDir, int yDir){
        if(xDir==0 && yDir==0) return 0;
        GameObject hit = GameManager.instance.SecretGrid[position.x+xDir, position.y+yDir].unit;
        if(hit==null){
            // move
            Step(xDir, yDir);
            int particle_check = PlayerPrefs.GetInt("Afterimage_Option");
            if((particle_check==1 || particle_check==3) && AfterimageParticles != null){
                GameObject particels = Instantiate(AfterimageParticles, transform.position, Quaternion.identity);
                particels.transform.SetParent(this.transform);
            }
            if(GameManager.instance.SecretGrid[position.x, position.y].misc.Count > 0){
                for(int i=0; i<GameManager.instance.SecretGrid[position.x, position.y].misc.Count; i++){
                    if(GameManager.instance.SecretGrid[position.x, position.y].misc[i] != null){
                        if (GameManager.instance.SecretGrid[position.x, position.y].misc[i].CompareTag("HP pickup")){
                            Destroy(GameManager.instance.SecretGrid[position.x, position.y].misc[i]);
                            GameManager.instance.SecretGrid[position.x, position.y].misc.RemoveAt(i);
                            GameManager.instance.PickedHealth(position.x - xDir, position.y - yDir);
                        }
                    }
                }
            }

            //Debug.Log(("Current pos", position.x, position.y));
            //Debug.Log(("Previous pos", previous_position.x, previous_position.y));

            PlayerManager.instance.PlayerPositions[PlayerManager.instance.cur_player] = new Position (position.x, position.y);
            return 1;
        }
        else if(hit.CompareTag("Enemy") || hit.CompareTag("Destructable")){
            // kill
            PlayerManager.instance.player_wait += 1;
            hit.GetComponent<Object>().Die(xDir, yDir);
            while(PlayerManager.instance.player_wait > 0);
            PlayerManager.instance.PlayerPositions[PlayerManager.instance.cur_player] = new Position (position.x, position.y);
            return 2;
        }
        // don't move
        PlayerManager.instance.PlayerPositions[PlayerManager.instance.cur_player] = new Position (position.x, position.y);
        return 0;
    }

    public int TheoreticalMove (int xDir, int yDir){
        if(xDir==0 && yDir==0) return 0;
        GameObject hit = GameManager.instance.SecretGrid[position.x+xDir, position.y+yDir].unit;
        if(hit==null)return 1;
        else if(hit.CompareTag("Enemy") || hit.CompareTag("Destructable"))return hit.GetComponent<Object>().ReturnExistance();
        else return 0;
    }

/*
    void LateUpdate(){
        if(!GameManager.instance.playersTurn) return;

        int x_move = 0;
        int y_move = 0;
        //x_move = (int) Input.GetAxisRaw("Horizontal");
        //y_move = (int) Input.GetAxisRaw("Vertical");
        if(Input.GetButtonDown("left")) x_move -= 1;
        else if(Input.GetButtonDown("right")) x_move += 1;
        if(Input.GetButtonDown("down")) y_move -= 1;
        else if(Input.GetButtonDown("up")) y_move += 1;

        if(x_move != 0) y_move = 0;

        if(x_move !=0 || y_move !=0){
            //Movin
            if(Move(x_move, y_move) == 1){
                // Success
                rotator.RotationBurst();
                GameManager.instance.PlayerPosition = new Position (position.x, position.y);
                GameManager.instance.playersTurn = false;
            }
            x_move = 0;
            y_move = 0;
        }
    }
*/

    public int MoveCommand(int x_move, int y_move){
        int result = TheoreticalMove(x_move, y_move);

        if(result > 0){
            // Success
            rotator.RotationBurst();
            //PlayerManager.instance.PlayerPositions[PlayerManager.instance.cur_player] = new Position (position.x, position.y);
            //GameManager.instance.playersTurn = 2;
            return result;
        }

        return 0;
    }
    

    public override void Die(int xDir, int yDir){
        // var shape = DeathParticles.GetComponent<ParticleSystem>().shape;
        GameObject particles = Instantiate(DeathParticles, transform.position, Quaternion.identity);
        particles.transform.SetParent(GameManager.instance.Particles_Players.transform);
        SoundManager.instance.PlaySound(death_sound);
        StartCoroutine(Camera.main.GetComponent<CameraShake>().ShakeRandom());
        //gameObject.SetActive(false);
        dying_done = true;
        Destroy(gameObject);
    }
}
