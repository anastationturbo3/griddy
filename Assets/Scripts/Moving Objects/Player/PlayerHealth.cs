﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public int health_num = 0;
    public Player player;
    public SpriteRenderer sprite;

    private Color alpha;

    void Awake(){
        player = transform.parent.gameObject.GetComponent<Player>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update(){
        alpha = sprite.color;
        //if(player.health < health_num) alpha.a = 0;
        //else alpha.a = 1;
        sprite.color = alpha;
    }
}