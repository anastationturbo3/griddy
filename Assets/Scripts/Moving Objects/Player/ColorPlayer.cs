using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPlayer : MonoBehaviour {
    public AnimationCurve color_curve;
    public Color base_color;

    public SpriteRenderer sprite;
    public float time = 0f;
    public float diff_rate = 0.1f;

    private Color true_color;
    private float original_diff_rate;

    void Awake(){
        sprite = GetComponent<SpriteRenderer>();
        original_diff_rate = diff_rate;
    }

    void FixedUpdate(){
        time += Time.deltaTime;
        true_color = base_color;
        true_color.a = color_curve.Evaluate(time);

        Color current_color = sprite.color;
        float r_diff = Mathf.Sign(current_color.r - true_color.r) * Mathf.Min(diff_rate, Mathf.Abs(current_color.r - true_color.r));
        float g_diff = Mathf.Sign(current_color.g - true_color.g) * Mathf.Min(diff_rate, Mathf.Abs(current_color.g - true_color.g));
        float b_diff = Mathf.Sign(current_color.b - true_color.b) * Mathf.Min(diff_rate, Mathf.Abs(current_color.b - true_color.b));
        float a_diff = Mathf.Sign(current_color.a - true_color.a) * Mathf.Min(diff_rate, Mathf.Abs(current_color.a - true_color.a));

        current_color = new Color(current_color.r - r_diff, current_color.g - g_diff, current_color.b - b_diff, current_color.a - a_diff);
        sprite.color = current_color;

        float diff_rate_diff = Mathf.Sign(diff_rate - original_diff_rate) * Mathf.Min(0.002f, Mathf.Abs(diff_rate - original_diff_rate));
        diff_rate -= diff_rate_diff;
    }
}
