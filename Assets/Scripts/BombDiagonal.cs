using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombDiagonal : MonoBehaviour {
    public GameObject Bleeper;
    public Position pos;
    public int set;

    private bool toDestroy = false;
    private GameObject Bleeper_Center;
    private List<GameObject> Bleepers_Up_Right = new List<GameObject>();
    private List<GameObject> Bleepers_Down_Right = new List<GameObject>();
    private List<GameObject> Bleepers_Up_Left = new List<GameObject>();
    private List<GameObject> Bleepers_Down_Left = new List<GameObject>();

    void Awake(){
        this.transform.SetParent(GameManager.instance.GetComponent<BoardManager>().boardHolder);
        GameManager.instance.Bombs.Add(this.gameObject);
        pos = new Position((int)transform.position.x, (int)transform.position.y);

        Bleeper_Center = Instantiate(Bleeper, new Vector3(pos.x, pos.y, 10), Quaternion.identity);
        GameManager.instance.SecretGrid[pos.x, pos.y].misc.Add(Bleeper_Center);
        Bleeper_Center.transform.SetParent(this.transform);
            
        int i=1;
        while(true){
            if(pos.x+i >= GameManager.instance.SecretGrid.GetLength(0) || pos.y+i >= GameManager.instance.SecretGrid.GetLength(1)) break;
            if(GameManager.instance.SecretGrid[pos.x+i, pos.y+i].tile == null) break;
            GameObject bleeper = Instantiate(Bleeper, new Vector3(pos.x+i, pos.y+i, 10), Quaternion.identity);
            bleeper.GetComponent<Bleeper>().delay = i;
            GameManager.instance.SecretGrid[pos.x+i, pos.y+i].misc.Add(bleeper);
            bleeper.transform.SetParent(this.transform);
            Bleepers_Up_Right.Add(bleeper);
            i++;
        }

        i=1;
        while(true){
            if(pos.x+i >= GameManager.instance.SecretGrid.GetLength(0) || pos.y-i < 0) break;
            if(GameManager.instance.SecretGrid[pos.x+i, pos.y-i].tile == null) break;
            GameObject bleeper = Instantiate(Bleeper, new Vector3(pos.x+i, pos.y-i, 10), Quaternion.identity);
            bleeper.GetComponent<Bleeper>().delay = i;
            GameManager.instance.SecretGrid[pos.x+i, pos.y-i].misc.Add(bleeper);
            bleeper.transform.SetParent(this.transform);
            Bleepers_Down_Right.Add(bleeper);
            i++;
        }

        i=1;
        while(true){
            if(pos.x-i < 0 || pos.y+i >= GameManager.instance.SecretGrid.GetLength(1)) break;
            if(GameManager.instance.SecretGrid[pos.x-i, pos.y+i].tile == null) break;
            GameObject bleeper = Instantiate(Bleeper, new Vector3(pos.x-i, pos.y+i, 10), Quaternion.identity);
            bleeper.GetComponent<Bleeper>().delay = i;
            GameManager.instance.SecretGrid[pos.x-i, pos.y+i].misc.Add(bleeper);
            bleeper.transform.SetParent(this.transform);
            Bleepers_Up_Left.Add(bleeper);
            i++;
        }

        i=1;
        while(true){
            if(pos.x-i < 0 || pos.y-i < 0) break;
            if(GameManager.instance.SecretGrid[pos.x-i, pos.y-i].tile == null) break;
            GameObject bleeper = Instantiate(Bleeper, new Vector3(pos.x-i, pos.y-i, 10), Quaternion.identity);
            bleeper.GetComponent<Bleeper>().delay = i;
            GameManager.instance.SecretGrid[pos.x-i, pos.y-i].misc.Add(bleeper);
            bleeper.transform.SetParent(this.transform);
            Bleepers_Down_Left.Add(bleeper);
            i++;
        }
    }

    public void Tick(){
        if(set == 0) Detonate();
        else set -= 1;
    }

    void Detonate(){
        /*
        Bleeper_Center.GetComponent<Bleeper>().KillSpot();
        foreach(GameObject bleeper in Bleepers_Up_Right) bleeper.GetComponent<Bleeper>().KillSpot();
        foreach(GameObject bleeper in Bleepers_Down_Right) bleeper.GetComponent<Bleeper>().KillSpot();
        foreach(GameObject bleeper in Bleepers_Up_Left) bleeper.GetComponent<Bleeper>().KillSpot();
        foreach(GameObject bleeper in Bleepers_Down_Left) bleeper.GetComponent<Bleeper>().KillSpot();
        */
        toDestroy = true;
    }

    void Update(){
        if(toDestroy && GameManager.instance.BobmsExplode && this.transform.childCount == 0){
            GameManager.instance.Bombs.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
    }
}
